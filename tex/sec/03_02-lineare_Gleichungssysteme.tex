\section{Lösbarkeit von linearen Gleichungssystemen}
\subsection{Lineare Gleichungssysteme}
\subsubsection{Definition}
Sei \(K\) ein Körper.
\begin{enumerate}
\item Ein Lineares Gleichungssystem (LGS) über \(K\) mit \(m\)
  Gleichungen für \(n\) Unbekannte \(x_1, \hdots, x_n\) hat die
  Form:\\
\[
  \begin{array}{c@{=}c}
    a_{11}\cdot x_1 + a_{12}\cdot x_2 + \hdots + a_{1n}\cdot x_n &b_1\\
    a_{21}\cdot x_1 + a_{22}\cdot x_2 + \hdots + a_{2n}\cdot x_n &b_2\\
    \vdots & \vdots\\
    a_{m1}\cdot x_1 + a_{m2}\cdot x_2 + \hdots + a_{mn}\cdot x_n &b_m\\

  \end{array}
  (*)
\]
wobei \(a_{ij}, b_i \in K\) für \(i = 1, \hdots, m, j = 1 \hdots n\).
\item Das Gleichungssystem \((*)\) heißt homogen, falls \(b_1 = \hdots
  = b_m = 0\), andernfalls inhomogen.
\item Die Matrix \(A = [a_{ij}]\in K^{m\times n}\) heißt
  Koeffizientenmatrix des LGS, der Vektor \(b = [b_{ij}]\in K^m\)
  heißt seine rechte Seite.
\item Der Vektor 
  \(x = 
  \begin{bmatrix}
    x_1\\ 
    \vdots\\ 
    x_n\\
  \end{bmatrix}
    \in K^n\) heißt
    Lösung des LGS \((*)\), falls \(Ax = b\).
\item \(L(A,b) = \{ x \in K^n | Ax = b\}\) heißt
  Lösungsmenge des LGS.
\end{enumerate}

\subsubsection{Programm}
    {\centering\includegraphics[width=14cm]{img/gauss_algorithmus.pdf}\\}

\subsubsection{Erinnerung}
\begin{align*}
  \mathscr{L}_A: K^n &\rightarrow K^m & \text{linear}\\
  x &\mapsto Ax
\end{align*}
\begin{align*}
  Kern(\mathscr{L}_A) &= \{x \in K^n | \mathscr{L}_A(x) = 0\}\\
  Bild(\mathscr{L}_A) &= \{b \in K^m | \exists x \in K^n:
                        \mathscr{L}_A(x) = b\}
\end{align*}
\subsubsection{Definition}
Sei \(A \in K^{m\times n}\). \\
Dann heißen:
\begin{align*}
Kern(A) &= \{x \in K^n|Ax = 0\} &(=Kern(\mathscr{L}_A))\\
Bild(A) &= \{b \in K^m|\exists x\in K^n: Ax = b\}&(=Bild(\mathscr{L}_A))\\
\end{align*}
Kern und Bild der Matrix \(A\).
\subsubsection{Bemerkungen}
\begin{enumerate}
  \item \(Kern(A)\subseteq K^n, Bild(A)\subseteq K^m\) ~\\
    sind Untervektorräume (Satz 2.2)
  \item \(dim(Kern(A)) + dim(Bild(A)) = dim(K^n) = n\) ~\\
    (Übung 7, PA 2i)
  \item \(Kern(A) = L (A, 0) \neq \varnothing \), da \(0\in L(A, 0)\)
  \item \(L(A, b)\neq \varnothing \Leftrightarrow b \in Bild(A)\)
    (Existenz der Lösung) 
  \item Ist \(x \in L(A, b)\), so ist \(L(A, b) = x + L(A,0) :=\{x +
    \tilde{x}|\tilde{x}\in L(A, 0)\}\)
    (affiner Untervektorraum)\\
    {\bf Beweis:}
    \begin{itemize}
    \item[\(\subseteq\)]:
      Sei \(y \in L (A, b) \Rightarrow A\cdot y = b\)
      \begin{align*}
        &\Rightarrow A(y-x) = Ay - Ax = b - b = 0 \\
        &\Rightarrow y - x \in L(A,0)\\
        &\Rightarrow y = x + (y-x) \in x + L(A,0)\\
        &\Rightarrow L(A, b) \subseteq x + L(A,0)
      \end{align*}

    \item[\(\supseteq\)]:
      Sei \(y = x + \tilde{x}\) mit \(\tilde{x} \in L(A, 0)\):\\
      \begin{align*}
        &\Rightarrow Ay = Ax + A\tilde{x} = b + 0 = b
        &\Rightarrow x \in L(A, b)
        &\Rightarrow x + L(A,0) \subseteq L(A,b)
      \end{align*}
      \textqed
    \end{itemize}
    \item \(\exists! x: Ax = b \Leftrightarrow L(A, 0) = \{0\}
      \xLeftrightarrow{S 2.2} \mathscr{L}_A \text{ - injektiv}\)
\end{enumerate}
\subsubsection{Satz 3.6}
Sei \(A = \begin{bmatrix}a_1& \hdots& a_n\end{bmatrix} \in K^{m \times n}\) mit \(a_i \in K^m,
i = 1, \hdots, n\) dann ist \(Bild(A) = Span(a_1, \hdots, a_n)\).
\subsubsection{Beweis}
\begin{align*}
  Bild(A) &= \left\{Ax \Bigg| x\in \begin{bmatrix} x_1\\ \vdots \\
      x_n\end{bmatrix} \in K^n\right\}\\
          &= \left\{\sum_{i=1}^n x_i a_i \Bigg| x_i \in K, i=1\hdots
            n\right\}\\
          &=Span(a_1, \hdots, a_n)
\end{align*}
\textqed
\subsubsection{Definition}
\begin{enumerate}
\item Der Rang (Spaltenrang) der Matrix \(A\in K^{m\times n}\) ist die
  Dimension des vom \(Bild(A)\), dh. \(Rang(A) = dim(Bild(A))\).
\item Der Zeilenrang der Matrix \(A\in K^{m\times n}\) ist der Rang
  von \(A^T\)
\end{enumerate}
\subsubsection{Beispiele}
\begin{enumerate}
  \item \(Rang(0_{m\times n}) = dim(Bild(0)) = dim(\{0\}) = 0\) 
  \item Sei \(r \in \mathbb{R}^n\)
    \begin{itemize}
      \item \(Rang (r) = 
        \begin{cases}
          1 & \text{ falls } r\neq 0\\
          0 & \text{ sonst}
        \end{cases}\)
      \item \(Bild(r) = \{rx|x \in \mathbb{R}\}\)
    \end{itemize}
  \item \(Rang(I_n) = dim(Bild(I_n)) = dim(K^n) = n\) 
  \item Sei \(A \in \mathscr{GL}_n(K)\Leftrightarrow Rang(A) = n\)
    \footnote{Dieser Beweis greift im Stoff voraus.}
    \begin{itemize}
      \item[\(\Rightarrow\)] Sei \(A \in \mathscr{GL}_n(K)\) beliebig
        \begin{align*}
          \Rightarrow
          & \exists A^{-1} \in \mathscr{GL}_n(K): 
                       A\cdot A^{-1} = I_n\\
          \xRightarrow{A \in \mathscr{GL}_n(K)}
          & dim(A) = dim(A\cdot A^{-1}) = dim(I_n) = n
        \end{align*}
      \item[\(\Leftarrow\)] 
        Sei \(A \in K^{n \times n} \land Rang(A) = n\)
        \begin{align*}
          \Rightarrow 
          &Rang(A) = n \land dim(Kern(A)) = 0\\
          \Rightarrow
          &L_A \text{ injektiv }\\
          \Rightarrow
          &\exists L_A^{-1} \in \mathscr{L}(K^m,K^n): L_A \circ L_A^{-1} = id\\
          \Rightarrow
          &\exists A^{-1}\in \mathscr{GL}_n(K):A \circ A^{-1} = I_n 
        \end{align*}
    \end{itemize}
  \item (Zeilenrang von A) = Spaltenrang von A = Rang(A)
\end{enumerate}
\subsubsection{Satz 3.7 (Existenz)}
Seien \(A \in K^{m \times n}, b \in K^m\) und \(\begin{bmatrix}A &b\\\end{bmatrix} \in K^{m \times
    (n + 1)}\).\\
    Das LGS \(Ax = b\) besitzt eine Lösung \(x \in K^n\) genau dann,
    wenn \(Rang(A) = Rang(\begin{bmatrix}A &b\\\end{bmatrix})\) 
\subsubsection{Beweis}
Es gilt \(Bild(A) \subseteq Bild(\begin{bmatrix}A&
  b\\\end{bmatrix})\), da \(u \in Bild(A)\ \Rightarrow \exists
  x \in K^n:u = Ax = Ax + b \cdot 0 =
  \begin{bmatrix}A &b\\\end{bmatrix} \begin{bmatrix}x
  \\0\\\end{bmatrix} \Rightarrow u \in Bild(\begin{bmatrix}A &
  b\\\end{bmatrix})\)
\begin{itemize}
  \item[\(\Rightarrow\)] Sei \(x \in K^n\) die Lösung von \(Ax =
    b\).\\
    \(z.z.: Bild(\begin{bmatrix}A &b\\\end{bmatrix})\subseteq Bild(A)\)~\\
    Sei \(u\in Bild(\begin{bmatrix}A &b\\\end{bmatrix})\):\\
    \begin{align*}
      &\Rightarrow \exists \begin{bmatrix}y\\z\\\end{bmatrix} \in
      K^{n+1}: u = \begin{bmatrix}A
        &b\\\end{bmatrix}\cdot \begin{bmatrix}y\\z\\\end{bmatrix} = Ay
      + bz = Ay + Axz = A(y + xz)\\
      &\Rightarrow u \in Bild(A)\\
      &\Rightarrow Bild(A) = Bild(\begin{bmatrix}A &b\\\end{bmatrix})\\
      &\Rightarrow Rang(\begin{bmatrix}A &b\\\end{bmatrix}) = Rang(A)
    \end{align*}
  \item[\(\Leftarrow\)]
    \begin{align*}
      Rang(A) = Rang(\begin{bmatrix}A &b\\\end{bmatrix})
              &\Rightarrow Bild(A) = Bild(\begin{bmatrix}A
                &b\\\end{bmatrix})\\
              &\Rightarrow b \in Bild(\begin{bmatrix}A
                &b\\\end{bmatrix}) = Bild(A)\\
              &\Rightarrow \exists x: Ax = b\\
    \end{align*}
    \textqed
\end{itemize}
\subsubsection{Satz 3.8 (Eindeutigkeit)}
Für \(A \in K^{m\times n}\) ist \(\mathscr{L}_A\) genau dann injektiv,
wenn \(Rang(A) = n\).

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Lineare_Algebra_1"
%%% End:
