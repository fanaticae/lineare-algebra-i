\section{Berechnung der Determinante}
\subsection{Laplace-Entwicklung}
\subsubsection{Algorithmus}
Die Entwicklung nach der \(i\)-ten Zeile:
\[det(A) = \sum_{j=1}^n (-1)^{i+j} a_{ij} det(A_{ij}), i = 1, \dots,n\]

\subsubsection{Beispiele}
\begin{itemize}
  \item \(n = 1: det(A) = a\)
  \item \(n = 2: det\left(
    \begin{bmatrix}
      a_{11} & a_{12}\\
      a_{21} & a_{22}\\
    \end{bmatrix}
    \right)
    = (-1)^2 a_{11}a_{22} + (-1)^3(a_{12}a_{21}) = a_{11}a_{22} - a_{12}a_{21}\)
  \item \(n = 3\)
    \begin{align*}
      det \left(
      \begin{bmatrix}
        a_{11} & a_{12} & a_{13}\\
        a_{21} & a_{22} & a_{23}\\
        a_{31} & a_{32} & a_{33}\\
      \end{bmatrix}\right)
      &= 
        a_{11}
        det \left(
        \begin{bmatrix}
          a_{22} & a_{23}\\
          a_{32} & a_{33}\\
        \end{bmatrix}\right)
      + a_{12}
        det \left(
        \begin{bmatrix}
          a_{21} & a_{23}\\
          a_{31} & a_{33}\\
        \end{bmatrix}\right)
      + a_{13}
        det \left(
        \begin{bmatrix}
          a_{21} & a_{22}\\
          a_{31} & a_{32}\\
        \end{bmatrix}\right)\\
      &=a_{11}a_{22}a_{33} - a_{11}a_{23}a_{32} - a_{12}a_{21}a_{33}
        + a_{12}a_{23}a_{31} + a_{13}a_{21}a_{32} - a_{13}a_{22}a_{31}
    \end{align*}
\end{itemize}
\subsubsection{Graphische Algorithmen}
{\centering\includegraphics[width=8cm]{img/berechnungdeterminante01.pdf}\\}
{\centering\includegraphics[width=8cm]{img/berechnungdeterminante02.pdf}\\}

\subsubsection{Satz 5.4}
Ist die Matrix 
\(A = 
\begin{bmatrix}
  a_{11} & \dots & a_{1n}\\
  \vdots & \ddots& \vdots\\
  a_{m1} & \dots & a_{nn}\\
\end{bmatrix}
\)
eine obere Dreiecksmatrix, so ist 
\[det(A) = a_{11} \cdot a_{22} \cdot \hdots a_{nn}\]

\subsubsection{Beweis}
\begin{align*}
  det(A) &= (-1)^{2n} \cdot a_{nn} \cdot 
           det\left(
           \begin{bmatrix}
             a_{11} & \hdots & a_{1(n-1)}\\
              & \ddots& \vdots\\
             0 &  & a_{(n-1)(n-1)}\\
           \end{bmatrix}
           \right)\\
  &= a_{nn}\cdot a_{(n-1)(n-1)} \cdot \cdots \cdot a_{22} \cdot a_{11}
\end{align*}

\subsubsection{Satz 5.5}
Sei \(A \in K^{n\times n}\). Die folgenden Aussagen sind äquivalent: 
\begin{enumerate}
  \item \(A\) ist invertierbar
  \item \(det(A) \neq 0\)
  \item \(Rang(A) = n\)
\end{enumerate}
Umgekehrt gilt auch: 
\begin{enumerate}
\item[1a.] \(A\) ist nicht invertierbar
\item[2a.] \(det(A) = 0\)
\item[3a.] \(Rang(A) < n\)
\end{enumerate}

\subsubsection{Beweis}
\begin{align*}
  A \text{ invertierbar }
  \Leftrightarrow&
  NSSF: A \cdot S_1\cdot \hdots \cdot S_q = I_n\\
  \Leftrightarrow&
  Rang(I_n) = n = Rang(A) \land 1 = det(I_n) = 
  n = (-1)^p \lambda_1,\hdots, \lambda_m det(A) 
  \text{ mit }\lambda_j \neq 0, j =1, \hdots, m \\
  \Leftrightarrow &
  det(A) \neq 0
\end{align*}

\subsubsection{Satz 5.6}
Es gilt \[det(A) = det(A^T)\] für alle \(A \in K^{n\times n}\)

\subsubsection{Beweis}
\begin{enumerate}
\item Ist \(A\) nicht invertierbar
  \begin{align*}
    \Rightarrow &A^T \text{ nicht invertierbar}\\
    \Rightarrow &det(A) = 0 = det(A^T)
  \end{align*}
\item Ist \(A\) invertierbar
  \begin{align*}
    \Rightarrow
    &A = S_q^{-1} \cdot \hdots \cdot S_1^{-1}\\
    \Rightarrow
    &A^T = S_1^{-T} \cdot \hdots \cdot S_q^{-T}
  \end{align*}
  \begin{enumerate}
  \item 
    \(S_k = P_{ij} 
    \Rightarrow S_k^{-1} = P_{ij} 
    \Rightarrow S_k^{-T} = P_{ij}
    \)
  \item 
    \(S_k = M_i(\lambda)
    \Rightarrow S_k^{-1} = M_i\left(\frac{1}{\lambda}\right)
    \Rightarrow S_k^{-T} = M_i\left(\frac{1}{\lambda}\right)
    \)
  \item 
    \(S_k = G_{ij}(\lambda)
    \Rightarrow S_k^{-1} = G_{ij}(-\lambda)
    \Rightarrow S_k^{-T} = G_{ji}(-\lambda)
    \)
  \end{enumerate}
  \begin{align*}
    \Rightarrow
    &det(A^T) = det(S_1^{-T}\cdot \hdots \cdot S_q^{-T}) 
      =(-1)^P \cdot \frac{1}{\lambda_1} \cdot \hdots \cdot
      \frac{1}{\lambda_m} \underbrace{I_n}_1\\
    &det(A) = det(S_q^{-1}\cdot \hdots \cdot S_1^{-1}) 
      =(-1)^p \cdot \frac{1}{\lambda_1} \cdot \hdots \cdot
      \frac{1}{\lambda_m}\cdot det(I_n)
  \end{align*}
\end{enumerate}

\subsubsection{Satz 5.7}
Für \(A, B \in K^{n\times n}\) gilt
\[det(AB) = det(A)det(B)\]

\subsubsection{Beweis}
\begin{itemize}
\item Ist \(B\) nicht invertierbar
  \begin{align*}
    \Rightarrow 
    &det(B) = 0 \land Rang(B) < n\\
    \Rightarrow 
    &Rang(AB) < n\\
    \Rightarrow
    &AB \text{ nicht invertierbar}\\
    \Rightarrow
    &det(AB) = 0 = det(B)
  \end{align*}
\item Ist \(B\) invertierbar
  \begin{align*}
    \Rightarrow
    &det(AB) = det(A \cdot \tilde{S}_q\cdot \hdots \cdot \tilde{S}_1) =
      \underbrace{(-1)^P \cdot \lambda_1 \cdot \hdots \cdot
      \lambda_m}_{det(B)}\cdot det(A)\\
    &\text{ Sei } A = I\\
    \Rightarrow
    &det(B) = (-1)^p \cdot \lambda_1\cdot \hdots \cdot \lambda_m 
  \end{align*}
\end{itemize}

\subsubsection{Korollar 5.8}
Seien \(A, B \in K^{n \times n}\)
\begin{enumerate}
\item \(det(AB) = det(BA)\) (Achtung: \(AB \neq BA\)!)
\item Für \(\lambda \in K\) gilt \(det(\lambda A) = \lambda^n det(A)\)
\item Ist \(A\) invertierbar, so gilt \[det(A^{-1}) =
    \frac{1}{det(A)}\]
  \(\left(\text{Achtung: } \frac{1}{A} \text{ existiert
        nicht!}\right)\)
\item Ist \(T\) invertierbar, so gilt \(det (T\cdot A \cdot T^{-1}) =
  det(A)\)
\item Elementare Spaltenumformungen ergeben die selben Änderungen der
  Determinanten wie die entsprechenden Zeilenumformungen, dh: 
  \begin{enumerate}
  \item \(det(A P_{ij}) = (-1)det(A)\)
  \item \(det(M_i(\lambda) A) = \lambda det(A)\)
  \item \(det(G_{ij}(\lambda)A) = det(A)\)
  \end{enumerate}
\item Es gilt die Laplace-Entwicklung nach der \(j\)-ten Spalte
  \[det(A) = \sum_{i=1}^n (-1)^{i+j} a_{ij} det(A_{ij})\]
\end{enumerate}

\subsubsection{Beweis}
\begin{enumerate}
\item \(det(AB) = det(A)det(B) = det(B)det(A) = det(BA)\)
\item \(det(\lambda A) = det\left(
    \begin{bmatrix}
      \lambda & &\\
      & \ddots & \\
      & & \lambda\\
    \end{bmatrix}
    \right)
    = det(\lambda I_n) det (A) = \lambda^n det(A)\)
\item \(1 = det(I_n) = det(A\cdot A^{-1}) = det(A) \cdot det(A^{-1})
  \Rightarrow det(A^{-1}) = \frac{1}{det(A)}\)
\item \(det(TAT^{-1})=det(T)det(A)det(T^{-1}) = det(A)\)
\item Sei \(S\) eine Elementarmatrix
  \[det(SA) = det(AS)\]
\item Seien \(A= [a_{ij}], A^T = [\tilde{a}_{ij}] = \tilde{A} \) mit
  \(\tilde{a}_{ij} = a_{ji}\)
  \begin{align*}
    det(A) 
    &= det(A^T)\\
    &= \sum_{j=1}^n (-1)^{i+j} \tilde{a}_{ij}det(\tilde{A}_{ij})\\
    &= \sum_{j=1}^n (-1)^{j+i} a_{ji} det(A_{ji})
  \end{align*}
\end{enumerate}

\subsubsection{Beispiel}
\begin{multicols}{2}
\(A = 
\begin{bmatrix}
  0 & 2 & -1 & 3\\
  0 & 5 & 0 & 8\\
  3 & 1 & -2& 4\\
  0 & 1 & 3 & 6\\
\end{bmatrix}
\)
\columnbreak
\begin{align*}
  det(A) 
  &= 3 \cdot det\left(
    \begin{bmatrix}
      2 & -1 & 3\\
      5 & 0 & 8\\
      1 & 3 & 6\\
    \end{bmatrix}
  \right)\\
  &=3\cdot \left(
    -5\cdot det\left(
    \begin{bmatrix}
      -1 & 3\\
      3 & 6\\
    \end{bmatrix}
   \right)
   - 8 \cdot det\left(
    \begin{bmatrix}
      2 & -1\\
      1 & 3\\
    \end{bmatrix}
   \right)
  \right)\\
  &= -15(-6-9) -24(6+1)\\
  &= 225 - 168\\
  &= 57
\end{align*}
\end{multicols}
\subsection{Komplementäre Matrix}
\subsubsection{Definition}
Sei \(A\in K^{n \times n}, n \geq 2\). Dann heißt die Matrix 
\[adj(A) = [\tilde{a}_{ij}]\in K^{n \times n}
 \text{ mit } \tilde{a}_{ij} = (-1)^{i+j} det(A_{ji})\] die
\emph{komplementäre Matrix} zu \(A\) 
\footnote{Auch \emph{Adjunkte Matrix} genannt}

\subsubsection{Satz 5.9}
Sei \(A \in K^{n \times n}, \ m,n \geq 2\). Dann gilt: 
\[A \cdot adj(A) = adj(A) \cdot A = det(A) \cdot I_n\]
Ist \(A\) invertierbar, so gilt 
\[A^{-1} = \frac{1}{det(A)} \cdot adj(A)\]

\subsubsection{Beweis}
Seien 
\(A = [a_{ij}], 
  adj(A)\cdot A = B = [b_{ij}], 
  adj(A) = [\tilde{a}_{ij}] \text{ mit } 
  \tilde{a}_{ij} = (-1)^{i+j} det(A_{ji})\)\\
Sei 
\(C_{ki} = 
\begin{bmatrix}
  a_1&\hdots&a_{i-1}&e_k& a_{i+1} & \hdots & a_n
\end{bmatrix}
\)
\[
  det(C_{ki}) 
  = (-1)^{k+i} \cdot 1 \cdot det(A_{ki})
  \Rightarrow det(A_{ki})
  = (-1)^{-k+i} \cdot det(C_{ki})
\]
\begin{align*}
  b_{ij} 
  &= \sum_{k=1}^n \tilde{a}_{ik} \cdot a_{kj}\\
  &= \sum_{k=1}^n \underbrace{(-1)^{i+k} \cdot
    det(A_{ki})}_{det(C_{ki})} \cdot a_{kj}\\
  &= \sum_{k=1}^n (-1)^{i+k} (-1)^{-(k+i)} 
    \cdot det(C_{ki}) \cdot a_{kj}\\
  &= \sum_{k=1}^n det(C_{ki}) \cdot A_{kj}\\
  &= \sum_{k=1}^n det\left(
    \begin{bmatrix}
      a_1 & \hdots & a_{i-1} & a_{kj}\cdot e_k & a_{i+1} & \hdots & a_n
    \end{bmatrix}\right)\\
  &= det\left(
    \begin{bmatrix}
      a_1 & \hdots & a_{i-1} & a_{j}& a_{i+1} & \hdots & a_n
    \end{bmatrix}\right)\\
  &=
    \begin{cases}
      0 & i\neq j\\
      det(A) & i = j
    \end{cases}\\
\Rightarrow& B = det(A)I
\end{align*}
\[
  \bordermatrix{
       & & & & &\cr
      j&\verbar{1cm}& \verbar{1cm}&\ast&\verbar{1cm} &
      \verbar{1cm}&\cr
  }
\]
Analog folgt: 
\[A \cdot adj(A) = det(A) \cdot I\]
Somit gilt:
\[Adj(A)\cdot A = det(A)\cdot A^{-1} \Rightarrow A^{-1} =
  \frac{1}{det(A)} \cdot adj(A)\]

\subsubsection{Beispiel}
Seien 
\(A = 
\begin{bmatrix}
  a & b\\
  c & d\\
\end{bmatrix}, 
det(A) = ad - bc,
adj(A) = 
\begin{bmatrix}
  d & -b\\
  -c& a\\
\end{bmatrix}
\)
\[\Rightarrow A^{-1} = \frac{1}{ad-bc} 
  \begin{bmatrix}
    d & -b\\
    -c & a\\
  \end{bmatrix}
\]
\subsubsection{Korollar 5.10 (Cramer'sche Formel)}
Sei \(A = \begin{bmatrix}a_1& \hdots& a_n\end{bmatrix} \in K^{n \times n}\) invertierbar und 
\(b = 
\begin{bmatrix}
  b_1\\
  \vdots\\
  b_n\\
\end{bmatrix}
\in K^n\). 
Dann ist 
\(x = 
\begin{bmatrix}
  x_1\\
  \vdots\\
  x_n\\
\end{bmatrix}
\)
eine eindeutige Lösung des LGS \(Ax=b\) gegeben durch 
\[x_i = \frac{det \left(
      \begin{bmatrix}
        a_1& \hdots& a_{i-1}& b & a_{i+1}& \hdots& a_n 
      \end{bmatrix}
      \right)}{det(A)}\]

\subsubsection{Beweis}
\begin{align*}
  x 
  &= A^{-1}\cdot b\\
  &= \frac{1}{det(A)} \cdot adj(A) \cdot b\\
  \Rightarrow x_i
  &= \frac{1}{det(A)} \sum_{k=1}^n(-a)^{i+k} det(A_{ki}) \cdot b_k\\
  &= \frac{1}{det(A)} \sum_{k=1}^n det\left(
    \begin{bmatrix}
      a_1 & \hdots & a_{i-1} & e_k & a_{i+1} & \hdots & a_n
    \end{bmatrix}\right)\\
  &= \frac{1}{det(A)} det \left(
    \begin{bmatrix}
      a_1 & \hdots & a_{i-1} & b & a_{i+1} & \hdots & a_n
    \end{bmatrix}\right)
\end{align*}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Lineare_Algebra_1"
%%% End:
