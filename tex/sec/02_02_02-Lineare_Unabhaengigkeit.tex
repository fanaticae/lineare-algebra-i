\subsection{Lineare Unabhängigkeit}
\subsubsection{Definition}
Sei \(V\) ein K-Vektorraum und seien 
\(v_1,\dots ,v_m \in V, m\geq 1\). 
Dann heißen \(v_1,\dots,v_m\) linear unabhängig, falls für alle
\(\lambda_1,\dots,\lambda_m\in K\) gilt:
\[\lambda_1 v_1 + \dots + \lambda_m v_m 
  = 0\Rightarrow \lambda_1 = \dots = \lambda_m = 0\]
Andernfalls heißen \(v_1, \dots v_m\) linear abhängig.

\subsubsection{Bemerkung}
\(v_1,\dots,v_m\) sind linear abhängig, wenn es 
\(\lambda_1, \dots, \lambda_m \in K\) gibt, 
die nicht alle gleich 0 sind, so dass
\(\lambda_1 v_1+ \dots + \lambda_m v_m = 0\).

\subsubsection{Beispiele}
\begin{enumerate}
  \item 
    \(\lambda_1\begin{bmatrix}1\\0\\0\\\end{bmatrix}+
    \lambda_2\begin{bmatrix}0\\1\\0\\\end{bmatrix}+
    \lambda_3\begin{bmatrix}0\\0\\1\\\end{bmatrix}=0
    \left(=\begin{bmatrix}0\\0\\0\\\end{bmatrix}\right)
    \Rightarrow \lambda_1 = \lambda_2 = \lambda_3 = 0\) ~\\
    \(\Rightarrow 
    \begin{bmatrix}1\\0\\0\\\end{bmatrix},
    \begin{bmatrix}0\\1\\0\\\end{bmatrix},
    \begin{bmatrix}0\\0\\1\\\end{bmatrix}\) sind linear unabhängig.\\
  \item \(w_1 + w_2 - w_3 = 0 \Rightarrow w_1, w_2, w_3\) 
    linear abhängig
  \item Sei \(0 \in V\) 
    \(\Rightarrow \exists \lambda_1 \neq 0: \lambda_1\cdot 0
    =0 \Rightarrow 0\) linear abhängig.
  \item Sei \(v\in V\backslash\{0\}\) ~\\
    Annahme: \(\lambda_1 \cdot v = 0\)
    \begin{align*}
      \Rightarrow &\lambda_1 = 0\\
      \Rightarrow & v  \text{ linear unabhängig}
    \end{align*}
  \end{enumerate}

\subsubsection{Satz}
Sei \(V\) ein K-Vektorraum. 
Die Vektoren \(v_1,\hdots,v_m \in V\) sind genau dann \emph{linear abhängig}, 
wenn ein \(v_i, i\in \{1,\hdots,m\}\) sich als Linearkombination der 
anderen Vektoren darstellen lässt.\\
\[v_i = \sum_{j = 1,j\neq i}^m \lambda_j v_i \text{ für }
  \lambda_j \in K, j=1\hdots m, j\neq i\]

\subsubsection{Beweis}
\begin{itemize}
\item \(\Rightarrow :\)
  Seien \(v_1, \hdots, v_m\) linear abhängig.
  \[\Rightarrow \underbrace{\exists \mu_1\hdots \mu_m}_{\text{nicht alle}=0
    }\in K: \sum_{j= 1}^m \mu_j v_j =0\]
  Sei also \(\mu_i \neq 0\), somit folgt:
  \begin{align*}
    \mu_i v_i &= -\sum_{\underset{j\neq i}{j=1}}^m\mu_jv_j\\
              \Rightarrow v_i &=\sum_{\underset{j \neq i}{j=1}}^m
                \left( -\frac{\mu_j} {\mu_i}\right) v_j
  \end{align*}
\item \(\Leftarrow\):
  Sei \(v_i = \sum\limits_{\underset{j\neq i}{j=1}}^m \lambda_jv_j\), somit
  folgt:
  \begin{align*}
    0 &= \sum_{j=1, j\neq i}^m\lambda_j v_j - v_i \\
      &=\sum_{i=1}^m
        \lambda_j v_j
        \text{ mit } \lambda_i = -1\neq 0
  \end{align*}
\(\Rightarrow v_1,\hdots, v_m\) linear abhängig.\\
\textqed
\end{itemize}
\subsubsection{Satz (2.4)}
Sei \(V\) ein K-Vektorraum, und seien \(v_1,\hdots, v_m \in V\) 
dann sind die folgenden Aussagen äquivalent:
\begin{enumerate}
  \item \(v_1,\hdots, v_m\) sind linear unabhängig
  \item Jeder Vektor \(v \in Span(v_1,\hdots,v_m) \) lässt sich in
    \emph{eindeutiger Weise} als Linearkombination von \(v_1,\hdots,v_m\)
    darstellen, dh:
    \[\forall \lambda, \mu \in K: \sum_{i = 1}^m\lambda_i v_i = \sum_{i = 1}^m
      \mu_i v_i \Rightarrow \lambda_i = \mu_i, i = 1\hdots m\]
  \item \(v_i \notin Span(v_1\hdots v_{i-1}, v_{i+1}, \hdots, v_m)\)
\end{enumerate}

\subsubsection{Beweis}
per Ringschluss.
\begin{itemize}
\item \(1\Rightarrow 2:\) Seien \(v_1,\hdots, v_m\) linear unabhängig
  und \(\lambda_i, \mu_i \in K, i = 1,\hdots, m\):
  \begin{align*}
    \sum_{i =  1}^m\lambda_i v_i = \sum_{i=1}^m \mu_i v_i
    &\Rightarrow \sum_{i = 1}^m (\lambda_i-\mu_i)v_i = 0\\
    &\Rightarrow \lambda_i - \mu_i = 0\\
    &\Rightarrow \mu_i = \lambda_i, i = 1\hdots m
  \end{align*}
\item \(2\Rightarrow 3:\)Angenommen es gibt \(i \in \{1,\hdots,m\}\)
  mit \(v_i \in Span(v_1,\hdots,v_{i-1}, v_{i+1},\hdots,v_m)\)  
  \begin{align*}
    \Rightarrow \exists \gamma_j:v_i
    &= \sum_{\underset{j\neq i}{j=i}}^m \gamma_jv_j+ 0 \cdot v_i
    &\sum_{j = 1}^m \lambda_j v_j\\
    &\lambda_j = 0, j\neq i\\
    &\lambda_i = 1\lightning_{\text{zu 2., da }\gamma_i = 0 \neq \lambda_i = 1}
  \end{align*}

\item \(3\Rightarrow 1:\) 
  Angenomen: \(v_1,\hdots, v_m\) linear abhängig
  \begin{align*}
    &\Rightarrow 
    \exists i \in \{1, \hdots, m\}: v_j = \sum_{\underset{j\neq i}{j=1}}^m \lambda_jv_j\\
    &\Rightarrow v_i \in Span(v_1, \hdots v_{i-1},v_{i+1},\hdots v_m)\\
    & \Rightarrow v_1, \hdots v_m \text{ linear unabhängig.}\\
  \end{align*}
\end{itemize}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Lineare_Algebra_1"
%%% End:
