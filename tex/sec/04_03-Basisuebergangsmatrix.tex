\subsection{Basisübergangsmatrix}
\subsubsection{Überlegung}
Sei \(V\) K-Vektorraum, \(dim(V) = n, B, \tilde{B}\) Basen von
\(V\).\\
Es sei \(\varphi_B(v)\) gegeben. Gesucht ist \(\varphi_{\tilde{B}}(v)\).
{\centering\includegraphics{img/komm_diagramm2.pdf}\\}
{\bf Oder kurz:}\\
{\centering\includegraphics{img/komm_diagramm3.pdf}\\}
\[\forall v\in V: \varphi_{\tilde{B}}(v) = [id]_{\tilde{B}}^B\]

\subsubsection{Definition}
Die Matrix \([id]_{\tilde{B}}^B\) heißt \emph{Basisübergangsmatrix}
von \(B\) nach \(\tilde{B}\)

\subsubsection{Beispiel}
Sei \(V = \mathbb{R}_2[x], B = {1, x, x^2}, 
\tilde{B} = [1, x+1,x^2+ x+ 1]\)
\begin{align*}
  id(1)  &= 1 = 1 + 0\cdot (x+1) + 0\cdot (x^2 + x + 1)\\
  id(x)  &= x = -1 \cdot 1 + 1 \cdot (x+1) + 0\cdot(x^2 + x +1)\\
  id(x^2)&= x^2=0 \cdot 1 + -1 \cdot (x+1) + 1\cdot(x^2 +x+1)
\end{align*}
Damit ist 
\[[id_v]_{\tilde{B}}^B = 
  \begin{bmatrix}
    1 & -1 &  0\\
    0 &  1 & -1\\
    0 &  0 &  1\\
  \end{bmatrix}
\]
Sei \(p(x)= - 3x^2 + 5x + 2\). 
Somit ist 
\(\varphi_B(p) = 
\begin{bmatrix}
  2\\
  5\\
  -3\\
\end{bmatrix}
\)
und 
\(\varphi_{\tilde{B}}(p) = 
\begin{bmatrix}
  1 & -1 &  0\\
  0 &  1 & -1\\
  0 &  0 &  1\\
\end{bmatrix}
\begin{bmatrix}
  2\\
  5\\
  -3\\
\end{bmatrix}
=
\begin{bmatrix}
  -3\\
  8\\
  -3\\
\end{bmatrix}
\)
\[(-3)1 + 8(x+1) + (-3)(x^2+x+1) = -3x^2 + 5x + 2 = p(x)\]

Ist \(f: V\rightarrow W\) und \(dim(V) = n\), \(dim(W) = m\).\\
Seien \(B_v, \tilde{B}_v\) Basen von \(V\), \(B_w, \tilde{B}_w\) Basen
von \(W\). 
{\centering\includegraphics[width=\linewidth]{img/komm_diagramm4.pdf}\\}
\[ [f]_{\tilde{B}_w}^{\tilde{B}_v} 
      = [id_w]_{\tilde{B}_w}^{B_w}
        \cdot[f]_{B_w}^{B_v}
        \cdot[id_w]_{B_v}^{\tilde{B}_v}
\]

\subsubsection{Satz (4.8) Transformation der Darstellungsmatrix bei
    Basiswechsel}
Sei \(f: V \rightarrow W\) linear und seien \(B_v, \tilde{B}_v\) Basen
von \(V\), \(B_w,\  \tilde{B}_w\) Basen von \(W\).\\
Dann gibt es invertierbare Matrizen \(T\) und \(S\) mit 
\[[f]_{\tilde{B}_v}^{\tilde{B}_w} = T [f]_{B_w}^{B_v} S\]

\subsubsection{Beweis}
Seien 
\(
  T = [id_w]_{\tilde{B_w}}^{B_w}, 
  S = [id_v]_{B_v}^{\tilde{B}_v}
\)
Dann gilt:
\[
  [f]_{\tilde{B}_w}^{\tilde{B}^v} = 
  [id_w\circ f \circ id_v]_{\tilde{B}_w}^{\tilde{B}_v} = 
  \underbrace{[id_w]_{\tilde{B}_w}^{B_w}}_T
    \cdot 
    [f]_{B_w}^{B_v}
    \cdot
    [id_v]_{B_v}^{\tilde{B}_v}
\]
\textqed

\subsubsection*{Bemerkung}
Die Reihenfolge der Basisvektoren ist
relevant für die Spalten der 
darstellenden Matrix

\subsubsection{Beispiel}
Seien \(V = \mathbb{R}_3[x], W=\mathbb{R}_2[x]\) mit 
\begin{align*}
D: V &\rightarrow W\\
   p &\mapsto p'
\end{align*}
Weiters seien 
\(
  B_v = \left\{1, x, x^2, x^3\right\},  
  \tilde{B}_v = \left\{1, x+1, x^2, x^3 + x^2\right\}
\)
Basen von \(V\)\\
und
\(
  B_w = \left\{1, x, x^2\right\},
 \tilde{B}_w = \left\{1, x+1, x^2 + x + 1\right\}
\)
Basen von \(W\).\\
Aus vorherigem Beispiel folgt: 
\begin{align*}
  [D]_{B_v}^{B_w} 
  &=\begin{bmatrix}
    0 & 1 & 0 & 0\\
    0 & 0 & 2 & 0\\
    0 & 0 & 0 & 3\\
    \end{bmatrix}
  &[id_w]_{\tilde{B}_w}^{B_w}
  &=\begin{bmatrix}
    1 & -1 & 0\\
    0 & 1 & -1\\
    0 & 0 & 1\\
    \end{bmatrix}
\end{align*}
Es fehlt also noch 
\[
  [id_v]_{B_w}^{\tilde{B}_w}
  = 
  \begin{bmatrix}
    1 & 1 & 0 & 0\\
    0 & 1 & 0 & 0\\
    0 & 0 & 1 & 1\\
    0 & 0 & 0 & 1\\
  \end{bmatrix}
\]
Somit folgt: 
\[\Rightarrow 
  [D]_{\tilde{B}_w}^{\tilde{B}_v}
  = 
  \begin{bmatrix}
    1 & -1 & 0\\
    0 & 1 & -1\\
    0 & 0 & 1 \\
  \end{bmatrix}
  \cdot 
  \begin{bmatrix}
    0 & 1 & 0 & 0\\
    0 & 0 & 2 & 0\\
    0 & 0 & 0 & 3\\
  \end{bmatrix}
  \cdot 
  \begin{bmatrix}
    1 & 1 & 0 & 0\\
    0 & 1 & 0 & 0\\
    0 & 0 & 1 & 1\\
    0 & 0 & 0 & 1\\
  \end{bmatrix}
  = 
  \begin{bmatrix}
    0 & 1 & -2 & -2\\
    0 & 0 & 2 & -1\\
    0 & 0 & 0 & 3\\
  \end{bmatrix}
\]
Überprüfung der Korrektheit (der ersten Spalte): 
\[0 \cdot 1 
  + 0 \cdot (x + 1) 
  + 0 \cdot (x^2 +x +1) 
  = 0_{\in W} = D(1)
  = 1' = 0\]

\subsubsection{Korollar (4.9)}
Sei \(f: V \rightarrow V\) linear und \(dim(V) = n\). 
Seien \(B\) und \(\tilde{B}\) zwei Basen von \(V\).\\
Dann gibt es invertierbare Matrix mit 
\[[f]_{\tilde{B}} = T\cdot [f]_B \cdot T^{-1}\]

\subsubsection{Beweis}
\begin{align*}
  [f]_{\tilde{B}} &= [id_v]_{\tilde{B}}^B [f]_B [id_v]_B^{\tilde{B}}
                = T [f]_B T^{-1}\\
  \underbrace{[id_v]_{\tilde{B}}^B}_T
  \cdot
  \underbrace{[id_v]_B^{\tilde{B}}}_{T^{-1}} 
  &= [id_v \circ id_v]_{\tilde{B}}^{\tilde{B}}
    = [id_v]_{\tilde{B}} = I_n
\end{align*}
\subsubsection{Definition}
Zwei Matrizen  \(A, \tilde{A} \in K^{m\times n}\) heißen
\emph{ähnlich}, wenn es eine invertierbare Matrix 
\(T \in K^{n \times n}\), sodass 
\[\tilde{A} = T \cdot A \cdot T^{-1}\]

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Lineare_Algebra_1"
%%% End:
