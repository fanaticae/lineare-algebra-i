\section{Abbildungen}
\subsection{Abbildungen}
\subsubsection{Definition}
Seien \(X,Y\) Mengen.\\
Eine Abbildung (oder Funktion) \(f\) von \(X\) nach \(Y\) ist eine
Vorschrift, die jedem \(x\in X\) genau ein Element \mbox{\(y = f(x) \in Y\)}
zuordnet. (\(\forall x \in X \exists! y \in Y: f(x) = y\)).\\
Jedes Element \(x\in X\) verweist also auf \emph{genau ein} \(y\in
Y\). Mehrere \(x\) dürfen auf ein \(y\) verweisen, nicht aber mehrere \(y\) von
einem \(x\) verwiesen werden.\\
Schreibweise:
\begin{align*}
  f: X &\rightarrow Y\\
  x &\mapsto y
\end{align*}
\subsubsection{Bemerkung}
Seien \(X,Y\) Mengen.\\
Eine Relation \(G_f \subseteq X\times Y\) heißt \emph{Graph} der
Abbildung \(f: X \rightarrow Y\), falls für jedes \(x \in X\) genau ein 
\(y \in Y\) mit der Eigenschaft \((x,y) \in G_f\) existiert.
\[
  G_f \subseteq X\times Y \text{ Graph von } f: X\rightarrow Y
  \Leftrightarrow 
  \forall x \in X \exists! y \in Y: (x,y) \in G_f
\]
\subsubsection{Beispiele}
\begin{enumerate}
  \item 
    \begin{align*}
      f: \mathbb{R} &\rightarrow \mathbb{R}\\
      x &\mapsto x^2
    \end{align*}
  \item 
    \begin{align*}
      f: \mathbb{R} &\rightarrow \mathbb{R}\\
      x&\mapsto 
         \begin{cases}
           1 &\text{falls } x>0\\
           0 &\text{falls } x=0\\
           -1&\text{falls } x<0
         \end{cases}
    \end{align*}
  \item 
    \begin{align*}
      +: \mathbb{R} \times \mathbb{R} & \rightarrow \mathbb{R}\\
      (x,y)&\mapsto x+y
    \end{align*}
  \item Identität:\\
    \begin{align*}
      id_X: X&\rightarrow X\\
      x &\mapsto x\\
      id_Y: Y&\rightarrow Y\\
      y &\mapsto y
    \end{align*}
  \item Tabellarisch \\
    Seien \(X = \{1,2,3\}, Y=\{a,b,c\}\) Mengen:\\
    \[
      \begin{array}{|c|c|}
        \hline
        x & f(x)\\
        \hline
        \hline
        1 & b\\
        \hline
        2 & b\\
        \hline
        3 & c\\
        \hline
      \end{array}
    \]
\end{enumerate}

\subsubsection{Bemerkung}
Seien \(f, g: X\rightarrow Y\) Abbildungen, so gilt:
\[
  f = g 
  \Leftrightarrow G_f = G_g
  \Leftrightarrow \forall x\in X: f(x) = g(x)
\]

\subsubsection{Definition}
Sei \(f: X\rightarrow Y\) eine Abbildung\\
Seien \(A\subseteq X, B\subseteq Y\) (Teil-)Mengen
\begin{enumerate}
  \item Bild von \(A\) unter \(f\):\\
    \[f(A) := \{f(x)|x\in A\}\]
  \item Urbild von \(B\) unter \(f\):\\
    \[f^{-1}(B):=\{x\in X| f(x) \in B\}\]
\end{enumerate}

\subsubsection{Beispiel}
Seien \(X = \{1,2,3\}, Y=\{a,b,c\}\) Mengen und sei 
\(f: X \rightarrow Y\) eine Abbildung mit:
\[
  \begin{array}{|c|c|}
    \hline
    x & f(x)\\
    \hline
    \hline
    1 & b\\
    \hline
    2 & b\\
    \hline
    3 & c\\
    \hline
  \end{array}
\]
so gilt:
\begin{itemize}
  \item \(f(\{1,2\}) = \{b\}\)
  \item \(f^{-1}(\{a,b\}) = \{1,2\}\)
  \item \(f^{-1}(\{a,b\}) = f^{-1}(\{b\})\)
\end{itemize}

\subsection{Eigenschaften von Abbildungen}
\subsubsection{Definition}
Seien \(X,Y\) Mengen. Eine Abbildung \(f:X\rightarrow Y\) heißt:
\begin{enumerate}
  \item \emph{injektiv}, falls
    \[\forall x, \tilde{x} \in X: f(x) = f(\tilde{x}) \Rightarrow x =
      \tilde{x}\]
  \item \emph{surjektiv}, falls
    \[\forall y \in Y \exists x: y=f(x)\]
  \item \emph{bijektiv}, falls die Abbildung surjektiv und injektiv ist. 
\end{enumerate}

\subsubsection{Bemerkung}
\begin{enumerate}
  \item \(f\) ist injektiv \(\Leftrightarrow\) jedes \(y\in Y\) hat
    höchstens ein Urbild unter \(f\). 
  \item \(f\) ist surjektiv \(\Leftrightarrow\) jedes \(y\in Y\) hat
    mindestens ein Urbild unter \(f\). 
  \item \(f\) ist bijektiv \(\Leftrightarrow\) jedes \(y\in Y\) hat
    genau ein Urbild unter \(f\). 
\end{enumerate}

\subsubsection{Beispiele}
\begin{enumerate}
  \item \(f:\mathbb{R}\rightarrow \mathbb{R}\,\text{ mit } f(x)=x^2\):
    \begin{itemize}
      \item nicht surjektiv, da: \(-1 = f(?)\)
      \item nicht injektiv, da: \(4 = f(2) = f(-2)\) 
    \end{itemize}
  \item \(f: \mathbb{R}\rightarrow [0,\infty)\,\text{ mit } f(x) = x^2\):
    \begin{itemize}
      \item surjektiv \(\forall y \in [0, \infty) \exists x: f(x) = y\)
      \item nicht injektiv, da \(4 = f(2) = f(-2)\)
    \end{itemize}
  \item \(f: [0,\infty) \rightarrow \mathbb{R}\,\text{ mit } f(x) = x^2\):
    \begin{itemize}
      \item nicht surjektiv, da: \(-2 = f(?)\)
      \item injektiv, da ein y keine zwei Urbilder hat.
    \end{itemize}
  \item \(f: [0,\infty) \rightarrow [0,\infty)\,\text{ mit } f(x) = x^2\):
    \begin{itemize}
      \item bijektiv
    \end{itemize}
\end{enumerate}

\subsection{Komposition zweier Abbildungen}
\subsubsection{Definition}
Seien \(X,Y,Z\) Mengen und \(f:X\rightarrow Y, g:Y\rightarrow Z\)
Abbildungen.\\
Dann heißt die Abbildung 
\begin{align*}
  g\circ f: X &\rightarrow Z \\
  x & \mapsto g(f(x))
\end{align*}
die Komposition (oder Hintereinanderausführung) von \(f\) und \(g\).

\subsubsection{Beispiele}
\begin{enumerate}
  \item Seien \(f, g: \mathbb{R} \rightarrow \mathbb{R}\) Abbildungen mit
    \(f(x) = x + 3\) und \(g(x) = x^2\):
    \begin{align*}
      (g\circ f)(x) &= g(f(x)) &(f\circ g)(x) &= f(g(x)) \\
      = g(x+3) &= (x+3)^2      &= f(x^2) &= x^2 + 3
    \end{align*}
  \item 
    \(g: \mathbb{R} \rightarrow \mathbb{R}\) mit 
    \(g(x) = \frac{1}{x}\) ist {\bf keine} Abbildung, da für \(x =
    0\) kein Wert definiert ist!\\
    Sei also \(g: \mathbb{R}\backslash\{0\} \rightarrow \mathbb{R}\)
    eine Abbildung mit \(g(x) = \frac{1}{x}\). 
    \begin{itemize}
      \item Sei \(f: \mathbb{R} \rightarrow \mathbb{R}\) eine Abbildung.\\
        So ist keine Abbildungskomposition \(g \circ f\) möglich, da
        \(f\) auf \(\mathbb{R}\) anstatt auf
        \(\mathbb{R}\backslash\{0\}\) abbildet.
      \item Sei 
        \(f: \mathbb{R} \rightarrow \mathbb{R}\backslash\{0\}\) ist
        die Komposition \(g \circ f\) möglich. 
    \end{itemize}
\end{enumerate}

\subsubsection{Bemerkungen}
Seien \(f: X \rightarrow Y, g: Y \rightarrow Z, h: Z \rightarrow W\)
Abbildungen: 
\begin{enumerate}
  \item \(h\circ (g\circ f) = (h\circ g) \circ f\) Assoziativgesetz\\
    {\bf Beweis:}
    Sei \(x \in X\) beliebig:
    \begin{align*}
      (h\circ (g\circ f)) (x) &= h(g(f(x)))\\
      ((h\circ g)\circ f) (x) &= (h\circ g)(f(x)) = h(g(f(x)))
    \end{align*}
    \textqed
  \item \(f\circ id_X = f, id_Y \circ f = f\) ~\\
    {\bf Beweis:}
    Sei \(x \in X\) beliebig:
    \begin{align*}
      (f \circ id_X)(x) &= f(id_X(x)) = f(x)\\
      (id_Y \circ f)(x)&= id_Y(f(x)) = f(x)\\
                       &\Rightarrow id_Y\circ f = f = f \circ id_X
    \end{align*}
\end{enumerate}

\subsubsection{Satz}
Die Abbildung \(f: X \rightarrow Y\) ist genau dann bijektiv, wenn es
genau eine Abbildung \(g: Y \rightarrow X\) mit \(g \circ f = id_X\) und
\(f\circ g = id_Y\) gibt.

\subsubsection{Beweis}
\begin{itemize}
  \item \(\Rightarrow\):\\
    Voraussetzung: \(f\) ist bijektiv \(\Rightarrow \forall y \in Y
    \exists x \in X: f(x) = y\).\\
    Definiere also \(g: Y \rightarrow X\) mit \( g(y) = x\).\\
    \(z.z.: g\circ f = id_X , f \circ g = id_Y\):
    \begin{align*}
      \forall x \in X: (g\circ f)(x) = g(f(x)) = g(y) = x 
      &\Rightarrow (g\circ f) = id_X\\
      \forall y \in Y: (f\circ g)(y) = f(g(y)) = f(x) = y
      &\Rightarrow (f\circ g) = id_Y\\
    \end{align*}
    {\bf Eindeutigkeit:}
    Seien \(g, \tilde{g}: Y \rightarrow X\) mit \((g\circ f) = id_X,
    (f \circ g) = id_Y\) und \((\tilde{g}\circ f) = id_X, (f \circ
    \tilde{g}) = id_Y\), so gilt:
    \[\tilde{g} = \tilde{g} \circ id_y = \tilde{g}\circ (f \circ g) =
      (\tilde{g}\circ f) \circ g = id_X \circ g = g\]
  \item \(\Leftarrow\):\\
    Voraussetzung: Es gibt \(g: Y \rightarrow X\) mit \(g\circ f = id_X\)
    und \(f\circ g = id_Y\)
    \begin{itemize}
      \item {\bf Injektivität:}\\
        \(z.z.: (g\circ f) = id_X \Rightarrow f \text{ injektiv}\) ~\\
        Seien \(x, \tilde{x} \in X\) mit \(f(x) = f(\tilde{x})\)
        \begin{align*}
          &\Rightarrow g(f(x)) = g(f(\tilde{x}))\\
          &\Leftrightarrow (g \circ f)(x) = (g \circ f)(\tilde{x})\\
          &\Leftrightarrow id_X (x) = id_X (\tilde{x})\\
          &\Rightarrow x = \tilde{x}
        \end{align*}
      \item {\bf Surjektivität:}\\
        \(z.z.: (f\circ g) = id_Y \Rightarrow f \text{ surjektiv}\) ~\\
        Sei \(y \in Y\) beliebig.
        \begin{align*}
          &\Rightarrow y = id_Y (y)\\
          &\Leftrightarrow y = (f\circ g)(y)\\
          &\Leftrightarrow y = f(g(y))\\
          &\Rightarrow \exists x \in X: g(y) = x\land y = f(x)
        \end{align*}
      \end{itemize}
      Somit ist \(f\) bijektiv.
      \textqed
\end{itemize}

\subsection{Inverse}
\subsubsection{Definition}
Ist \(f: X \rightarrow Y\) bijektiv, so heißt die eindeutig bestimmte
Abbildung \(g: Y \rightarrow X\) mit den Eigenschaften 
\(g \circ f = id_X, f\circ g = id_Y\) die Umkehrabbildung oder Inverse
von \(f\).\\
Geschrieben wird \(f^{-1}\), nicht gleichzusetzen mit
\(\frac{1}{f}\).

\subsubsection{Beispiel}
\begin{align*}
  f: [0, \infty) &\rightarrow [0, \infty) &f(x) &= x^2\\
  f^{-1}: [0,\infty)&\rightarrow [0,\infty)&f^{-1}(x) &= \sqrt{x}
\end{align*}
\subsubsection{Satz}
Seien \(f: X \rightarrow Y, g: Y \rightarrow Z\) bijektive Abbildungen
\begin{enumerate}
  \item \((f^{-1})^{-1} = f\)
  \item \((g \circ f )^{-1} = f^{-1} \circ g^{-1}\)
\end{enumerate}

\subsubsection{Beweis}
\begin{enumerate}
  \item \(z.z.: (f^{-1})^{-1} = f\) ~\\
    Sei \(f^{-1}: Y \rightarrow X\) Abbildung, \((f^{-1})^{-1}:X
    \rightarrow Y\) die Inverse zu \(f^{-1}\) und \(f^{-1}\) die
    Inverse zu \(f: X\rightarrow Y\).
    \[\forall x \in X:  (f^{-1})^{-1} (x) = ((f^{-1})^{-1} \circ id_X)(x)
      = ((f^{-1})^{-1} \circ f^{-1} \circ f) (x) = (id_Y \circ f) (x)
      = f (x)\]
  \item \(z.z.: (g\circ f)^{-1} = f^{-1} \circ g^{-1}\)
    Seien \(f: X\rightarrow Y\) und \(g: Y \rightarrow Z\)
    umkehrbare Abbildungen.
    \begin{align*}
      f^{-1} \circ g^{-1} \circ (g\circ f) = 
      &f^{-1} \circ (g^{-1} \circ g) \circ f = f^{-1} \circ id_Y \circ f
      = f^{-1} \circ f = id_X\\
      (g\circ f) \circ (f^{-1} \circ g^{-1}) =
      &g\circ (f \circ f^{-1}) \circ g^{-1} = g \circ id_Y \circ
        g^{-1} = id_Z    
    \end{align*}
\end{enumerate}



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Lineare_Algebra_1"
%%% End:
