\section{Determinante}
\subsection{Ziel}
\(A \in K^{m\times n}\).
Bestimme anhand einer Abbildung in den
Körper bestimmte Eigenschaften einer Matrix.
\[det(A) = 0 \Leftrightarrow A \text{ ist nicht invertierbar}\]

\subsection{Definition}
Wir betrachten den Spezialfall 
\(
  n = 2, 
  A = [a_1, a_2]
  =
  \begin{bmatrix}
    a_{11} & a_{12}\\
    a_{21} & a_{22}\\
  \end{bmatrix}
\)
\[
  A \text{ invertierbar }
  \Leftrightarrow
  a_1 \text{ und } a_2
  \text{ linear unabhängig }
  \Leftrightarrow 
  \text{ Flächeninhalt } F 
  \text{ des Parallelograms mit Seiten }
  a_1 \text { und } a_2 \text{ ist nicht }
  0
\]
{\centering\includegraphics[width=.25\linewidth]{img/paralellogram1.pdf}\\}
\[F = |D(a_1, a_2)| \neq 0\]

\subsubsection{Idee}
Konstruiere eine Funktion 
\(D: \mathbb{R}^{2\times 2} \rightarrow \mathbb{R}\) 
mit deren Hilfe man den Flächeninhalt bestimmen kann. Falls es so ein
\(D\) gibt, sollte es folgende Eigenschaften haben: 
\begin{itemize}
  \item \((ML)\) Multilinearität: Linearität in jeder Komponente
    \begin{align*}
      D(a_1 + \tilde{a}_1, a_2) &= D(a_1, a_2) + D(\tilde{a}_1, a_2)\\
      D(a_1, a_2 + \tilde{a}_2) &= D(a_1, a_2) + D(a_1, \tilde{a_2})\\
      D(\lambda a_1, a_2) &= \lambda D(a_1, a_2)\\
      D(a_1, \lambda a_2) &= \lambda D(a_1, a_2)
    \end{align*}
    {\centering\includegraphics[width=.25\linewidth]{img/paralellogram2.pdf}\\}
  \item \((AB)\) Alterniertheitsbedingung: 
    \begin{align*}
      &D(a_1, a_1) = 0\\
      \Rightarrow
      &D(a_1, a_2) = -D(a_2, a_1)
    \end{align*}
    {\bf Beweis:}
    \begin{align*}
      0 
      &\overset{AB}{=} D(a_1 + a_2, a_1 + a_2)\\
      &\overset{ML}{=} D(a_1, a_1 + a_2) + D(a_2, a_1 + a_2)\\
      &\overset{ML} = D(a_1, a_1) + D(a_1, a_2)+D(a_2, a_1)+D(a_2,a_2)\\
      &\overset{AB} = D(a_1, a_2) + D(a_2, a_1)
    \end{align*}
  \item \((NB)\) Normierungsbedingung: \(D(e_1, e_2) = 1\)
    \begin{align*}
      \Rightarrow D(a_1, a_2) 
      &= D(a_{11}e_1 + a_{21}e_2, a_{12}e_1 + a_{22}e_2)\\
      &=a_{11}a_{12} \underbrace{D(e_1, e_1)}_0 
        + a_{21}a_{12} \underbrace{D(e_2, e_1)}_{-1}
        + a_{12}a_{21} \underbrace{D(e_2, e_2)}_0 
        + a_{11}a_{22} \underbrace{D(e_1, e_2)}_{1}\\
      &=a_{11}a_{22}-a_{12}a_{21}\\
      D &= |a_{11}a_{22} -a_{12} a_{21}| &&\text{(LAII)}
    \end{align*}
\end{itemize}
\subsubsection{Definition}
Eine Abbildung \(det: K^{n \times n} \rightarrow K\) heißt
Determinante,\footnote{Oft wird statt \(det(A)\) auch \(|A|\)
    geschrieben}
 falls folgende Eigenschaften erfüllt sind: 
\begin{itemize}
  \item \((ML)\): \(det\) ist linear bezüglich jeder Spalte von \(A\).\\
    dh: 
    \begin{enumerate}
      \item Linearität bezüglich Addition
        \[
          det([a_1, \hdots, a_{i-1}, a_i + \tilde{a}_i, a_{i + 1},
          \hdots, a_n]) = 
          det([a_1, \hdots, a_{i-1}, a_i, a_{i+1}, \hdots, a_n])
          + 
          det([a_1, \hdots, a_{i-1},\tilde{a}_i, a_{i+1}, \hdots, a_n])
        \]
      \item Linearität bezüglich Skalarmultiplikation
        \[
          det([a_1, \hdots, a_{i-1}, \lambda a_i, a_{i + 1},
          \hdots, a_n]) = 
          \lambda det([a_1, \hdots, a_{i-1}, a_i, a_{i+1}, \hdots, a_n])
        \]
    \end{enumerate}
  \item \((AB)\): \(det\) ist alternierend, dh \(det(A) = 0\),
    falls \(A\) zwei identische Spalten hat.
  \item \((NB)\): \(det\) ist normiert, dh \(det(I) = 1\)
\end{itemize}

\subsubsection{Lemma (5.1)}
Sei \(det: K^{n\times n } \rightarrow K, n \geq 2\) eine
Determinante.
\begin{enumerate}
  \item Für die Permutationsmatrix 
    \[P_{ij} = 
      [e_1, \hdots, e_{i-1}, e_j, e_{i+1}, \hdots
      e_{j-1}, e_i, e_{j+1}, \hdots, e_n]\]
    mit \(i < j\)
    gilt
    \[det(A P_{ij}) = -det(A)\]
  \item Für 
    \[M_i(\lambda) = 
      [e_1, \hdots, e_{i-1}, \lambda e_i, e_{i + 1}, \hdots, e_n]
    \]
    gilt: 
    \[det(A M_i(\lambda)) = \lambda det(A)\]
  \item Für 
    \[G_{ij} = [e_1, \hdots, e_{j-1}, e_j + \lambda e_i, e_{j + 1},
      \hdots, e_n]\] 
    gilt 
    \[det(A G_{ij}(\lambda)) = det (A)\]
\end{enumerate}

\subsubsection{Beweis}
\begin{enumerate}
\item 
  \begin{align*}
    0 &= det([a_1, \hdots, a_{i-1}, 0, a_{i + 1}, \hdots, a_n])\\
      &= det([a_1, \hdots, a_{i-1}, a - a, a_{i + 1}, \hdots, a_n])\\
      &= det([a_1, \hdots, a_{i-1}, a, a_{i+1}, \hdots, a_n])  
       -  det([a_1, \hdots, a_{i-1}, a, a_{i+1}, \hdots, a_n]) \\
      &= 0
  \end{align*}
\item 
  \[det(A M_i(\lambda)) = det\left(
      \begin{bmatrix}
        a_1& \hdots& a_{i-1}&\lambda a_i& a_{i + 1}& \hdots& a_n\\
      \end{bmatrix}
    \right)
    \overset{ML}{=}
    \lambda det
    \left(
      \begin{bmatrix}
        a_1 & \hdots & a_{i-1} & a_i & a_{i+1} & \hdots & a_n
      \end{bmatrix}
    \right)
  \]
\item 
  \begin{align*}
    det(G_{ij}) 
    &= det([a_1, \hdots, a_{j-1},a_{j} + \lambda a_i , a_{j + 1}, \hdots, a_n])\\
    &= det(A) + det([a_1, \hdots, a_{j-1},\lambda a_i,a_{j+1}, \hdots, a_n])\\
    &= det([a_1, \hdots, a_{i-1}, a, a_{i+1}, \hdots, a_n])
      +\lambda \underbrace{det([a_1, \hdots, a_{j-1}, a_i, a_{j+1}, \hdots, a_n])}_0\\
    &= det([a_1, \hdots, a_{i-1}, a, a_{i+1}, \hdots, a_n])
  \end{align*}
\end{enumerate}

\subsubsection{Definition}
Sei \(A \in K^{n \times n}\) mit \(n \geq 2\), Dann definieren wir
\(A_{ij} \in K^{(n-1)\times(n-1)}\) als die Matrix, die aus \(A\)
durch Streichen der i-ten Zeile und j-ten Spalte entsteht.\\
\[det(A_{ij}) \text{ heißt \emph{Minor} der } (n-1)\text{-ten Ordnung
      von }A\]

{\centering\includegraphics[width=8cm]{img/matrix_minor.pdf}\\}


\subsubsection{Satz (5.3) Existenz und Eindeutigkeit der Determinante}
Es gibt genau eine Abbildung \(det: K^{n \times n} \rightarrow K\),
die die Bedingungen
\begin{itemize}
  \item Multilinearität \((ML)\)
  \item Alterniertheit \((AB)\)
  \item Normierungsbedingung \((NB)\)
\end{itemize}
erfüllt. 


\subsubsection{Beweis}
\begin{itemize}
  \item {\bf Eindeutigkeit:} Seien 
    \(det, \tilde{det}: K^{n\times n}\rightarrow n\), sodass 
    \((ML),(AB),(NB)\) erfüllt sind. Sei \(A \in K^{n \times n}\)\\
    Dann kann \(A\) durch elementare Spaltenumformungen in die
    \emph{Normierte Spaltenstufenform (NSSF)} 
    \(\tilde{A} = A \cdot S_1 \cdot \hdots \cdot S_q\) mit 
    \(S_1, \hdots, S_q \in K^{n \times n}\) Elementarmatrizen.
    \begin{align*}
      \xRightarrow{L 6.1}
      &det(\tilde{A}) = (-1)^p \lambda_1 \cdot \hdots \cdot \lambda_m
        det(A)\\
      \Rightarrow
      &\tilde{det}(\tilde{A}) = (-1)^p \lambda_1 \cdot \hdots \cdot
        \lambda_m \tilde{det}(A)
    \end{align*}
    Wobei \(p\) bzw \(m\) die Anzahl der Multiplikationen mit
    \(P_{ij}\) bzw \(M_i \lambda_j\) mit \(\lambda_1, \hdots, \lambda_m
    \in K\backslash\{0\}\) sind.
    \begin{itemize}
      \item Ist \(A\) nicht invertierbar
        \begin{align*}
          \Rightarrow
          &\tilde{A} \text{ hat eine Nullspalte}\\
          \Rightarrow 
          &det(\tilde{A}) = 0 = \tilde{det}(\tilde{A})\\
          \Rightarrow
          &det(A) = \tilde{det}(\tilde{A})
        \end{align*}
      \item Ist \(A\) invertierbar
        \begin{align*}
          \Rightarrow
          &\tilde{A} = I_n &&\text{Analog NZSF}\\
          \Rightarrow
          &det(\tilde{A}) = 1 = \tilde{det}(\tilde{A})\\
          \Rightarrow
          &det(A) = \tilde{det}(A)\\
          \Rightarrow 
          &det = \tilde{det}
        \end{align*}
    \end{itemize}
  \item {\bf Existenz:} Per Induktion nach \(n\)
    \begin{itemize}
      \item {\bf IA:} \(n = 2\)\\
        \begin{align*}
          det: K^{2\times 2} & \rightarrow K\\
          \begin{bmatrix}
            a_{11} & a_{12}\\
            a_{21} & a_{22}\\
          \end{bmatrix}
          &\mapsto a_{11}a_{22} - a_{12}a_{21}
        \end{align*}
        erfüllt \((ML),(AB),(NB)\)
      \item {\bf IV:} Angenommen, es existiert für \(n-1\) bereits
        eine Funktion \(f: K^{(n-1)\times (n-1)} \rightarrow K\) mit
        \((ML),(AB),(NB)\)
      \item {\bf IS}
        Sei \(A \in K^{n \times n}\) und sei \(i \in \{1, \hdots,
        n\}\). 
        \[det(A) = \sum_{j=1}^n (-1)^{i+j} a_{ij}f(A_{ij})\]
        \begin{itemize}
          \item \((ML)\): Seien
            \begin{align*}
              A &= [a_1, \hdots, a_{k-1}, a_k, a_{k+1}, \hdots, a_n]\\
              B &= [a_1, \hdots, a_{k-1}, b_k, a_{k+1}, \hdots, a_n]\\
              C &= [a_1, \hdots, a_{k-1}, a_k + b_k, a_{k+1}, \hdots,
                  a_n]\\
              G &= [a_1, \hdots, a_{k-1},\lambda a_k, a_{k+1}, \hdots,
                  a_n]
            \end{align*}
            \(z.z.: det(C) = det(A) + det(B)\)
            \begin{align*}
              det(C)
              &=\sum_{j=1}^n (-1)^{i+j} c_{ij} f(C_{ij})\\
              &=\left(
                \sum_{\underset{j \neq k}{j=1}}^n
                (-1)^{i+j}\underbrace{c_{ij}}_{a_{ij}} f(C_{ij})
                \right)
                + (-1)^{i+k} (a_{ik} + b_{ik}) f(C_{ik})\\
              &=\sum_{\underset{j \neq k}{j=1}}^n (-1)^{i+j}
                \underbrace{a_{ij}}_{b_{ij}} 
                \left( f(A_{ij}) + f(B_{ij})\right)
                + (-1)^{i + k} a_{ik} f(A_{ij}) + b_{ik} f(B_{ij})\\
              &=\sum_{j=1}^n (-1)^{i + j} a_{ij} f(A_{ij}) 
               +\sum_{j=1}^n (-1)^{i + j} b_{ij} f(B_{ij})
            \end{align*}
            {\centering\includegraphics[width=8cm]{img/determinante01.pdf}\\}
            \(z.z.: det(G)= \lambda det(A)\)
            \begin{align*}
              det(G) 
              &= \sum_{j=1}^n (-1)^{i+j} g_{ij} f(G_{ij})\\
              &= \sum_{\underset{j\neq k}{j=1}}^n (-1)^{i + j}
                \underbrace{g_{ij}}_{a_{ij}}
                \underbrace{f(G_{ij})}_{\lambda f(A_{ij})}\\
              &= \lambda det(A)
            \end{align*}
          \item \((NB)\): Seien \(I_n = 
            \begin{bmatrix}
              1 &  &0\\
                &\ddots &\\
              0 & & 1\\
            \end{bmatrix}
            = [\delta_{ij}], 
            \delta_{ij} = 
            \begin{cases}
              1 & i = j\\
              0 & \text{ sonst}\\
            \end{cases}
            \)
            \begin{align*}
              det(I_n) 
              &= \sum_{j = 1}^n (-1)^{i + j} \delta_{ij} f((I_n)_{ij})\\
              &= (-1)^{2i}\delta_{ii} f(I_{n-1})\\
              &= 1
            \end{align*}
          \item \((AB)\):
            Seien \(k\)-te und \(l\)-te Spalten von \(A\) gleich und
            \(k < l\).
            \begin{align*}
              det(A) 
              &= \sum_{j=1}^n (-1)^{i+j}a_{ij}f(A_{ij})\\
              &= \sum_{\underset{\stackrel{j \neq k}{j\neq l}}{j=1}}^n (-1)^{i+j} a_{ij}
                \underbrace{f(A_{ij})}_{0}
                + (-1)^{i + k} a_{ik} f(A_{ik}) + (-1)^{i+l} a_{il} f(A_{il})\\
              A_{ik} 
              &= [a_1, \hdots,a_{k-1}, a_{k+1}, \hdots,a_{l-1}, a_{l},
                a_{l+1} \hdots, a_n]\\
              A_{il}
              &= [a_1, \hdots,a_{k-1},\underbrace{a_{l}}_{=a_{k}}, a_{k+1}, \hdots,a_{l-1},
                   a_{l+1} \hdots, a_n]\\
              &= [a_1, \hdots,a_{k-1}, a_{k+1},a_{l}, a_{k+2} \hdots,a_{l-1},
                   a_{l+1} \hdots, a_n] \cdot P_{k, k + 1}\\
              &= [a_1, \hdots,a_{k-1}, a_{k+1}, \hdots,a_{l-1},
                   a_l, a_{l+1} \hdots, a_n]\cdot P_{l, l -1}\cdot \hdots \cdot P_{k, k + 1}\\
              \Rightarrow det(A) 
              &= (-1)^{i+k} a_{ik} f(A_{ik}) + (-1)^{i + l} a_{ik} 
                \underbrace{f(A_{il})}_{(-1)^{l-1-k} f(A_{ik})}\\
              &= (-1)^i a_{ik} f(A_{ik}) \cdot\underbrace{(-1)^k -
                (-1)^k)}_0 \\
              &= 0
            \end{align*}
            {\centering\includegraphics[width=8cm]{img/determinante02.pdf}\\}
        \end{itemize}
    \end{itemize}
\end{itemize}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Lineare_Algebra_1"
%%% End:
