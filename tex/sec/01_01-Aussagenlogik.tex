\section{Aussagenlogik}
\subsection{Aussagen}
\subsubsection{Definition}
Eine \emph{Aussage} ist eine \emph{sprachliche Einheit}, die einen
Sachverhalt ausdrückt, welchem \emph{genau einer} der beiden Wahrheitswerte
(wahr, falsch) zugeordnet werden kann.

\subsubsection{Beispiele}
\begin{tabular}{|c|c|p{5cm}|}
\hline 
{\bf Aussage} & {\bf Zustand} & {\bf Kommentar}\\
\hline
Augsburg ist eine Stadt & Wahre Aussage &\\
\hline 
\(3+7=11\) & Falsche Aussage&\\
\hline
FCA! FCA! & Keine Aussage & Es kann kein Wahrheitswert zugeordnet
                            werden.\\
\hline 
Auf dem Saturn gibt es Lebewesen & Aussage, unbekannter Wahrheitswert
                                   & Der Aussage kann ein eindeutiger
                                     Wahrheitswert zugeordnet werden,
                                     dies ist aber nach momentanem
                                     Wissensstand nicht möglich.\\
\hline 
\(x^2 - 1 = 0\) & Keine Aussage (Aussageform) & Eine Aussage, da von
                                                einer Unbekannten
                                                (\(x\)) abhängig. Wird
                                                als Aussageform
                                                bezeichnet.\\
\hline
\end{tabular}
\subsection{Aussageform}
\subsubsection{Definition}
Eine \emph{Aussageform} ist ein Satz, der \emph{mindestens eine
  Variable} enthält.
\subsubsection{Beispiel}
\begin{enumerate}
\item \(\underbrace{ A \left( x \right) }_{\text{Abhängig von x}} : x^2 -1 = 0\) \\
  \begin{tabular}{|c|c|}
    \hline
    {\bf Aussage} & {\bf Zustand}\\
    \hline 
    \(A(1): 1^2 -1 = 0\) & Wahre Aussage\\
    \hline
    \(A(5): 5^2 -1 = 0\) & Falsche Aussage\\
    \hline
  \end{tabular}
\item \(B\left(x,y\right):\) \(5x\) ist durch \(y\) teilbar.
\end{enumerate}

\subsection{Quantoren}
\subsubsection{Allquantor $\forall$}
\glqq Für \emph{alle x} gilt A(x)\grqq\ \(\Leftrightarrow\forall x:
A\left( x \right)\).\\

z.B:
 \(\forall x: x^2 -1 = 0\) ist eine falsche Aussage, da:
 \(5^2 - 1 \neq 0\)
\subsubsection{Existensquantor $\exists $}
\glqq Es existiert mindestens ein x mit A(x) \grqq\
 \(\Leftrightarrow\exists x: A\left( x \right) \).\\

z.B: 
 \(\exists x: x^2-1 = 0\) 
 ist eine wahre Aussage, da
 \(1^2 - 1 = 0, (-1)^2 -1 = 0\). 

\subsubsection{Eindeutigkeitsquantor $\exists ! $}
\glqq Es existiert \emph{genau ein} x mit A(x)\grqq\
 \(\Leftrightarrow\exists ! x: A\left( x \right)\)\\

z.B:
\(
\begin{array}{l@{\,
\,}l}
 \exists ! x: x-1 = 0  &\text{ ist Wahre Aussage}\\
 \exists ! x: x^2 - 1 = 0& \text{ ist Falsche Aussage}\\
\end{array}
\)
\subsubsection{Quantorenreihenfolge}
Die Reihenfolge der Quantoren ist relevant für die Bedeutung der
Aussage.\\

z.B:\\
\indent Sei \(x\) Mann und \(y\) Frau.\\
\indent \(B(x,y)\): \(x\) kann mit \(y\) glücklich sein.\\

\begin{tabular}{c p{5cm}}
{\bf Term} &{\bf Aussage}\\
\(\forall x\exists y: B(x,y)\) & Zu jedem Mann \(x\) gibt es eine Frau
                                 \(y\) mit der er glücklich werden
                                 kann.\\
\(\exists y\forall x: B(x,y)\) & Es existiert mindestens eine Frau
                                 \(y\) mit der jeder Mann \(x\)
                                 glücklich werden kann.\\
\end{tabular}

\subsection{Junktoren/Aussagenverknüpfungen}
\subsubsection{Junktoren}
\begin{enumerate}
  \item Negation \(\lnot\)\\
    Man beachte im besonderen, dass bei Quantoren gilt:
    \begin{align*}
      \lnot(\forall x: A(x)) &\Leftrightarrow \exists x:\lnot A(x)\\
      \lnot(\exists x: A(x)) &\Leftrightarrow \forall x:\lnot A(x) 
    \end{align*}
  \item Konjunktion \(\land\) 
  \item Adjunktion/Disjunktion \(\lor\) 
  \item Implikation \(\Rightarrow\)\\ 
    Für \(A \Rightarrow B\) spricht man:
    \begin{itemize}
      \item Aus A folgt B
      \item A impliziert B
      \item Wenn A dann B 
      \item A ist hinreichend für B
      \item B ist notwendig für A
    \end{itemize}
  \item Äquivalenz \(\Leftrightarrow\)\\
    Für \(A \Leftrightarrow B\) spricht man:
    \begin{itemize}
      \item A ist äquivalent zu B   
      \item A genau dann wenn B
      \item A ist hinreichend und notwendig für B 
        (\((A \Rightarrow B) \land (A \Leftarrow B)\)) 
    \end{itemize}
\end{enumerate}

\subsubsection{Wahrheitstabellen}
\[
\begin{array}{|c|c||c|c|c|c|c|}
  \hline
  A & B & \lnot A & A \land B & A \lor B & A \Rightarrow B & A \Leftrightarrow B\\
  \hline
  \hline
  w & w & f & w & w & w & w\\
  \hline
  w & f & f & f & w & f & f\\
  \hline
  f & w & w & f & w & w & f\\
  \hline 
  f & f & w & f & f & w & w\\
  \hline
\end{array}
\]
\subsubsection{Beispiele}
\begin{itemize}
  \item Implikation: \(1+1 = 0 \Rightarrow 2=2\).\\
    Beweis:\\
    Wenn \(1+1 = 0\) , dann gilt \(0 = 1+1\), also gilt:\\
    \(1+1 = 1+1\).\textqed
  \item Äquivalenz\\
    1.1.2222 ist ein Montag \(\Leftrightarrow\) 2.1.2222 ist ein
    Dienstag.
\end{itemize}

\subsubsection{Satz}
Folgende Aussagen sind für beliebige Aussagen A und B immer wahr
(\emph{Tautologien}):

\begin{enumerate}
  \item \(\lnot(\lnot A) \Leftrightarrow A\):\\
    Doppelte Negation 
  \item \(A \land (\lnot A) \Rightarrow B\):\\
    \glqq Aus Unsinn kann man alles folgern\grqq
  \item \(\lnot (A \land (\lnot A))\):\\
    Ausgeschlossener Widerspruch
  \item \(\lnot (A \lor B) \Leftrightarrow (\lnot A)\land (\lnot B)\):\\
    DeMorgan'sche Regel
  \item \(\lnot(A \land B) \Leftrightarrow (\lnot A)\lor (\lnot B)\):\\
    DeMorgan'sche Regel
  \item \(A \Rightarrow B \Leftrightarrow (\lnot B) \Rightarrow (\lnot
    A)\):\\
    Kontraposition.
\end{enumerate}
\subsubsection{Beweis}
\begin{enumerate}
  \item[6.] Kontraposition:
    \[
    \begin{array}{|c|c|c|c||c|c|}
      \hline
      A & B & \lnot A & \lnot B & A\Rightarrow B & \lnot B \Rightarrow \lnot A\\
      \hline 
      w & w & f & f & w & w\\
      \hline 
      w & f & f & w & f & f\\
      \hline
      f & w & w & f & w & w\\
      \hline 
      f & f & w & w & w & w\\
      \hline 
    \end{array}
    \]
\end{enumerate}

\subsubsection{Beispiel}
\begin{enumerate}
  \item[6.] Kontraposition:\\
    Behauptung in der Form 
    \(\forall n \in \mathbb{N}: A(n) \Rightarrow B(n)\):\\
    Für alle \(n\in \mathbb{N}\) gilt: 
    \(n^2\) gerade \(\Rightarrow n\) gerade\\
    {\bf Beweis}:\\
    \(\lnot B:\) Sei \(n\) ungerade.\\
    \begin{align*}
      &\Rightarrow \exists k \in \mathbb{N}_0: n = 2 \cdot k+ 1\\
      &\Rightarrow n^2 = (2k + 1)^2 = 4k^2 +4k + 1 = 2\underbrace{(2k^2 +
        2k)}_{\in \mathbb{N}} + 1\\
      &\Rightarrow n^2 \text{ ungerade } (\lnot A).
    \end{align*}
\end{enumerate}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Lineare_Algebra_1"
%%% End:

