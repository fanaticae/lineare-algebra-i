\section{Ringe}
\subsection{Ring}
\subsubsection{Definition}
Sei \(R \neq \varnothing\) Menge mit Verknüpfungen \(+,\cdot\).\\
Das Tripel \((R,+,\cdot)\) heißt Ring, falls
\begin{itemize}
  \item \((R,+)\) eine kommutative Gruppe ist
  \item Es gilt das Assoziativgesetz 
    \[\forall a,b,c\in R: (a\cdot b) \cdot c = a \cdot (b\cdot c)\]
  \item Es gelten die Distributivgesetze:
    \begin{align*}
      \forall a,b,c\in R: &&a(b+c) &= a\cdot b + c\cdot d\\
                          &&(a+b)c &= ac+bc
    \end{align*}
\end{itemize}

Der Ring \((R,+,\cdot)\) heißt
\begin{itemize}
  \item \emph{Ring mit Eins}, falls
    \[\exists e \in R: e\cdot a = a\cdot e = a\]
    (\(e = 1\): Neutrales Element).
  \item \emph{kommutativ}, falls
    \[\forall a, b \in R: a\cdot b = b\cdot a \]

  \item \emph{nullteilerfrei}, falls:
    \[\forall a,b\in R: a\cdot b = 0 \Rightarrow a = 0\lor b = 0\]
\end{itemize}

\subsubsection{Beispiele}
\begin{tabular}{l l}
  \((\mathbb{Q},+,\cdot)\) & Ring\\
  \((\mathbb{Z}, +, \cdot)\) & kommutativer, nullteilerfreier Ring
    mit Eins\\
\(M =\{0, \pm 2, \pm 4, \dots\}\) & kommutativer, nullteilerfreier
    Ring OHNE Eins\\
\end{tabular}

\subsubsection{Satz}
Sei \((R, +, \cdot)\) ein kommutativer, nullteilerfreier Ring mit Eins
mit endlich vielen Elementen, dann ist \((R,+,\cdot)\) ein Körper. 

\subsubsection{Beweis}
\(z.z.: \forall a\in R \backslash \{0\} \exists a'\in R\backslash\{0\}: 
  a' \cdot a  = 1\) ~\\
Sei \(\varphi: R\rightarrow R\) mit \(\varphi(x) = a\cdot x\) eine
Abbildung.\\
\(z.z.: \varphi \text{ injektiv}\).
Seien \(x_1, x_2\in R\), 
so dass \(\varphi(x_1) = \varphi(x_2)\).
\begin{align*}
  \Rightarrow
  & ax_1 = ax_2\\
  \Rightarrow
  & ax_1 - ax_2 = a(x_1 - x_2)\\
  \Rightarrow
  & x_1 = x_2 &\text{Da R nullteilerfrei und }a \neq 0\\
  \Rightarrow
  & \varphi \text{ injektiv}
\end{align*}
Da \(R\) endlich viele Elemente hat \(\Rightarrow \varphi(R) = R\).\\
Da \(\varphi\) injektiv und bei Abbildung jedes Element abgebildet
werden muss (Wohldefiniertheit), kann \(R\) als endliche Menge nur
surjektiv sein.
\[\Rightarrow \exists a'\in R: 
  1 = \varphi(a') \land a' \in R\backslash\{0\}\]
\(a \cdot a'  = \varphi(a') = 1\)
\footnote{Falls \(a' = 0 \Rightarrow a\cdot a' = 
    a \cdot 0 = 0\lightning_{a\cdot a'\neq 1}\)}
\textqed\\
\(\Rightarrow (R, +, \cdot) \) ist Körper

\subsubsection{Bemerkung}
Die Aussage ist falsch für die Ringe mit unendlich vielen Elementen
(\(R =\mathbb{Z}\)).

\subsubsection {Beispiel}
\(
  (\mathbb{Z}_m,+),
  R=\{(x,y)\in \mathbb{Z}\times \mathbb{Z}|
  m \text{ teilt } x-y\},
  \mathbb{Z}_m = \{[0]_R, [1]_R, [2]_R, \dots, [m-1]_R\}
\)
\begin{align*}
  +: \mathbb{Z}_m \times \mathbb{Z}_m &\rightarrow \mathbb{Z}_m 
  &\cdot: \mathbb{Z}_m\times \mathbb{Z}_m &\rightarrow \mathbb{Z}_m\\
  ([x]_R, [y]_R) &\mapsto [x+y]_R
  &([x]_R, [y]_R) &\mapsto [x\cdot y]_R
\end{align*}
Seien \([x]_R = [x']_R, [y]_R = [y']_R\)
\begin{itemize}
  \item Wohldefiniertheit
    \begin{align*}
      &\Rightarrow m \text{ teilt } x-x' \land m \text{ teilt } y-y'\\
      &\Rightarrow x\cdot y - x'\cdot y' = xy - x'y + x'y - x'y' = (x-x')y +
        x'(y-y')\\
      &\Rightarrow m \text{ teilt } x\cdot y - x'\cdot y'
      &\Rightarrow [xy] = [x'y'] 
    \end{align*}
  \item Assoziativgesetz 
    \begin{align*}
      ([x]_R [y]_R)[z]_R &= [xy]_R [z]_R \\
                         &=[(xy)z]_R\\
                         &=[x(yz)]_R\\
                         &=[x]_R [yz]_R\\
                         &=[x]_R ([y]_R [z]_R)
    \end{align*}
  \item Distributivgesetze
    folgen analog
  \item Kommutativität
    \[[x]_R [y]_R  = [xy]_R = [yx]_R = [y]_R [x]_R\]
  \item Neutrales Element bezüglich Multiplikation: \([1]_R\)
    \[\forall[x]_R \in \mathbb{Z}_m: [1]_R [x]_R = [1x]_R = [x]_R\]
  \item Nullteilerfreiheit:
    Der Ring \((\mathbb{Z}_m, + , \cdot)\) ist genau dann nullteilerfrei,
    wenn \(m\) eine Primzahl ist.\\
    {\bf Beweis:}
    \begin{itemize}
    \item[\(\Rightarrow\)]:
      Sei \((\mathbb{Z}_m, +, \cdot)\) nullteilerfrei 
      \(\Rightarrow ([x]_R [y]_R = [0]_R
        \Rightarrow [x]_R = [0]_R \lor [y]_R = [0]_R)\)
      und sei \(m\) keine Primzahl 
      \(\Rightarrow m = n_1 \cdot n_2\) mit
      \(n_1, n_2 \in \mathbb{N}\backslash\{0\}\)
      \begin{align*}
        &\Rightarrow 1 < n_1 < m, 1< n_1 < m\\
        &\Rightarrow [n_1]_R[n_2]_R = [n_1n_2]_R = [m]_R 
          = 0_R \land [n_1]_R \neq [0]\land [n_2]\neq [0]\lightning
      \end{align*}
      Da \(m|(m-0) \Rightarrow \frac{m}{m} = 1\) mit 0
      Rest. \(\Rightarrow m\) Primzahl
    \item[\(\Leftarrow\)]:
      Sei \(m\) eine Primzahl und \([x]_R [y]_R = [0]_R\)
      \begin{align*}
        &\Rightarrow [xy]_R = [0]_R &&\text{dh. }m \text{ teilt } xy\\
        &\Rightarrow m|x \lor m|y &&\\
        &\Rightarrow [x]_R = [0]_R \lor [y]_R = [0]_R&&\\
        &\Rightarrow (\mathbb{Z}_m, +, \cdot) \text{ Nullteilerfrei}
      \end{align*}
  \end{itemize}
\end{itemize}



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Lineare_Algebra_1"
%%% End:
