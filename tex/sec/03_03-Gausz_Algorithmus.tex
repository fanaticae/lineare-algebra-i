\section{Der Gauß-Algorithmus}
\subsection{Algorithmus}
\subsubsection{Ziel}
Gegeben sei das lineare Gleichungssystem \(Ax = b\) mit \(A\in
K^{m\times n}, b \in K^m\). Bestimme \(L(A,b)\)
\subsubsection{Beispiel}
Löse \(Ax = b\) mit 
\(A = 
  \begin{bmatrix}
    5 & 4 & 1\\
    0 & 4 & 5\\
    0 & 0 & 6\\
  \end{bmatrix}, 
  b = 
  \begin{bmatrix}
    21\\
    14\\
    12\\
  \end{bmatrix},
  x =
  \begin{bmatrix}
    x_1\\
    x_2\\
    x_3\\
  \end{bmatrix}
\)
\begin{align*}
  5 x_1 - 4 x_2 + x_3 &= 21 &\xRightarrow{3.}& &5 x_1 + 4 + 2 &= 31
  &\Rightarrow& & x_1 &= 3\\
  4 x_2 + 5 x_3 &= 14 &\xRightarrow{2.}& &4x_2 + 10 &=14 &\Rightarrow&&
    x_2 &= 1\\
  6x_3 &= 12 &\xRightarrow{1.}& &x_3 &= 1
\end{align*}
\begin{enumerate}
\item Es lässt sich der Wert von \(x_3\) errechnen
\item Durch Einsetzen von \(x_3\) in die höheren Reihen, lässt sich
  der Wert von \(x_2\) errechnen.
\item Der Wert von \(x_1\) ergibt sich durch die eingesetzten Werte
  von \(x_2\) und \(x_3\).
\end{enumerate}
Im Allgemeinen lassen sich derartige Probleme also durch \emph{Rückeinsetzen} lösen. 

\subsubsection{Allgemein}
\(a_{ii} \neq 0, i = 1\dots n\):\\
\[
  \begin{bmatrix}
    a_{11} & a_{12} &\dots & a_{1n}\\
           & a_{22} &\dots & a_{2n}\\
           &        &\ddots& \vdots\\
       0   &        &      & a_{nn}\\
  \end{bmatrix}
  \cdot
  \begin{bmatrix}
   x_1\\\vdots\\\vdots\\x_n\\
  \end{bmatrix}
  = 
  \begin{bmatrix}
   b_1\\\vdots\\\vdots\\b_n\\
  \end{bmatrix}
 \]
\begin{itemize}
\item[] n-te Zeile:\\
  \[x_n = \frac{b_n}{a_{nn}}\]
\item[] (n-1)-te Zeile:\\
 Da \(a_{n-1,n-1} x_{n-1} + a_{n-1,n} x_n = b_{n-1}\)\\
 \[\Rightarrow x_{n-1} = \frac{1}{a_{n-1,n-1}}\left( b_{n-1} - a_{n-1,n} x_n \right)\]
\item[] 1-te Zeile:\\
 Da \(a_{11}\cdot x_1 + a_{12}\cdot x_2 + \dots + a_{1n}x_n = b_1\)\\
 \[\Rightarrow x_1 = \frac{1}{a_{11}} \cdot \left(b_1 - a_{12}x_2 -\dots - a_{1n}x_n\right) \]
\item[] \emph{Lösung}:
  \[x = 
    \begin{bmatrix}
      x_1\\\vdots\\x_n\\
    \end{bmatrix}
    \text{ mit }
    x_i = \frac{1}{a_{ii}}\left(b_i - \sum\limits_{j = i + 1}^n
  a_{ij}x_i \right)\]
 für \(i = n \dots 1\).

\end{itemize}
Ist selbstverständlich auch mit rechteckigen Matrizen möglich.

\subsubsection{Satz 3.9}
Seien \(A\in K^{m\times n}, b \in K^m, S\in K^{m\times m}\).\\
Dann gilt:
\begin{enumerate}
  \item \(L(A,b) \subseteq L(SA, Sb)\)
  \item Ist \(S\) invertierbar, so gilt \(L(A,b) = L(SA, Sb)\)
\end{enumerate}
\subsubsection{Beweis}
\begin{enumerate}
\item Sei \(x \in L(A,b) \Rightarrow Ax = b \Rightarrow SAx = Sb
  \Rightarrow x\in L(SA, Sb)\)
\item 
  \begin{itemize}
    \item[\(\subseteq\)] folgt aus 1.
    \item[\(\supseteq\)] Sei \(x \in L(SA, Sb) \Rightarrow SAx = Sb
          \Rightarrow \underbrace{S^{-1}S}_{I_m}Ax =
          \underbrace{S^{-1}S}_{I_m} b \Rightarrow x \in L(A,b)\)
  \end{itemize}
\end{enumerate}
\subsection{Elementarmatrizen}
\subsubsection{Definition}
Die folgenden Matrizen aus \(K^{m\times n}\) heißen
Elementarmatrizen:
\begin{enumerate}
\item[]
  \begin{enumerate}
    \item 
      \(P_{ij}:= 
        \begin{bmatrix}
          e_1& \hdots& e_{i-1}& e_j& e_{i+1}& \hdots& e_{j-1}& e_i&
          e_{j+1}& \hdots e_m\\
        \end{bmatrix}
        \text{ für } m \geq 2, i < j\) (Permutationsmatrix)
    \item 
      \(M_i(\lambda):= 
        \begin{bmatrix}
          e_1& \hdots & e_{i-1} & \lambda e_i & e_{i + 1} &\hdots &
          e_m\\
        \end{bmatrix}
        \text{ für } \lambda \in K \backslash\{0\}\)
    \item
      \(G_{ij}(\lambda):=I_m + \lambda E_{ij} = 
        \begin{bmatrix}
          e_1& \hdots& e_{j-1}& e_j + \lambda e_i& e_{j+1}& \hdots& e_m\\
        \end{bmatrix}
        \text{ für } \lambda \in K, n \geq 2, i\neq j\)
  \end{enumerate}
\end{enumerate}
Hierbei ist \(e_i \in K^m\) der i-te Einheitsvektor und 
\( E_{ij}\in K^{m\times n}, 
   E_{ij} = 
    \bordermatrix{
         & &     &   j &     & \cr
         &0&\hdots&\hdots&\hdots&0\cr
        i&\vdots&\hdots&1&\hdots&\vdots\cr
         &0&\hdots&\hdots&\hdots&0\cr
    }
\)

\subsubsection{Bemerkungen}
\begin{enumerate}
\item Die Elementarmatrizen sind invertierbar und es gilt:
  \begin{enumerate}
    \item \(P_{ij}^{-1} = P_{ij}\) 
    \item \(M_i(\lambda)^{-1} = M_i\left(\frac{1}{\lambda}\right)\)
    \item \(G_{ij}(\lambda)^{-1} = G_{ij}(-\lambda)\)
  \end{enumerate}
  (Beweis als Übung)

\item Multiplikation von links mit einer Elementarmatrix bewirkt die
  folgenden elementaren Zeilenumformungen:
  \begin{enumerate}
    \item \(P_{ij}\): Vertauschen der Zeilen i und j:\\
      \scalebox{.5}{
      \(\bordermatrix{
          &  &  &  & i &  &  &  &  & j&  & &\cr
          &1 &  &  &  &  &  &  &  &  &  & &\cr 
          & &\ddots  &  &  &  &  &  &  &  &  & &\cr 
          & &  &1  &  &  &  &  &  &  &  & &\cr 
         i& &  &  &0  &  &  &  &  &1  &  & &\cr 
          & &  &  &  &1  &  &  &  &  &  & &\cr 
          & &  &  &  &  &\ddots  &  &  &  &  & &\cr 
          & &  &  &  &  &  &  &1  &  &  & &\cr 
         j& &  &  &1 &  &  &  &  &0  &  & &\cr 
          & &  &  &  &  &  &  &  &  &1  & &\cr 
          & &  &  &  &  &  &  &  &  &  &\ddots& \cr 
          & &  &  &  & & & & & & & &1\cr
      }\cdot 
      \bordermatrix{
          &  &  &  & i &  &  &  &  & j&  & &\cr
          & &  &  &  &  &*  &  &  &  &  & &\cr 
          & &  &  &  &  &  &  &  &  &  & &\cr 
          & &  &  &  &  &  &  &  &  &  & &\cr 
         i&a_{i1}&  &  &\hdots  &  &  &\hdots  &  &  &\hdots  & &a_{in}\cr 
          & &  &  &  &  &  &  &  &  &  & &\cr 
          &\vdots &  &  &*  &  &  &  &  &  &*  & &\vdots\cr 
          & &  &  &  &  &  &  &  &  &  & &\cr 
         j&a_{j1} &  &  &\hdots  &  &  &\hdots  &  &  &\hdots  & &a_{jn}\cr 
          & &  &  &  &  &  &  &  &  &  & &\cr 
          & &  &  &  &  & * &  &  &  &  & \cr 
          & &  &  &  & & & & & & & &\cr
      }
      = 
      \bordermatrix{
          &  &  &  & i &  &  &  &  & j&  & &\cr
          & &  &  &  &  &*  &  &  &  &  & &\cr 
          & &  &  &  &  &  &  &  &  &  & &\cr 
          & &  &  &  &  &  &  &  &  &  & &\cr 
         i&a_{j1}&  &  &\hdots  &  &  &\hdots  &  &  &\hdots  & &a_{jn}\cr 
          & &  &  &  &  &  &  &  &  &  & &\cr 
          &\vdots &  &  &*  &  &  &  &  &  &*  & &\vdots\cr 
          & &  &  &  &  &  &  &  &  &  & &\cr 
         j&a_{i1} &  &  &\hdots  &  &  &\hdots  &  &  &\hdots  & &a_{in}\cr 
          & &  &  &  &  &  &  &  &  &  & &\cr 
          & &  &  &  &  & * &  &  &  &  & \cr 
          & &  &  &  & & & & & & & &\cr
      }
      \)
      }
      \item \(M_i(\lambda)\): Multiplikation der i-ten Zeile mit
        \(\lambda\):\\
        \scalebox{.7}{
        \(
          \bordermatrix{
              & & & &i & \cr 
              & 1 \cr
              & &\ddots\cr
              & & &1\cr
             i& & & &\lambda\cr
              & & & & & 1\cr
              & & & & & &\ddots\cr
              & & & & & & & 1\cr
          }\cdot
          \bordermatrix{
              & & & & & \cr 
              & & & & & \cr
              & & & &*& \cr
              & & &\cr
             i&a_{i1} & & &\hdots & & &a_{in}\cr
              & & & & & \cr
              & & & &*& & \cr
              & & & & & & & \cr
          }
          = 
          \bordermatrix{
              & & & & & \cr 
              & & & & & \cr
              & & & &*& \cr
              & & &\cr
             i&\lambda a_{i1} & & &\hdots & & &\lambda a_{in}\cr
              & & & & & \cr
              & & & &*& & \cr
              & & & & & & & \cr
          }
          \)
          }
        \item \(G_{ij}(\lambda)\): Addition des \(\lambda\)-fachen der
          j-ten Zeile zur i-ten Zeile:\\
          
        \scalebox{.7}{
        \(
          \bordermatrix{
              & & & & & &j \cr 
              & 1 \cr
              & &\ddots\cr
             i& & & &1& &\lambda\cr
              & & & & & \ddots\cr
              & & & & & & 1\cr
              & & & & & & &\ddots\cr
              & & & & & & & & 1\cr
          }\cdot
          \bordermatrix{
              & & & & & \cr 
              & & & &*& \cr
             i&a_{i1} & & &\hdots & & &a_{in}\cr
              & & & &*& \cr
              & & &\cr
             j&a_{j1} & & &\hdots & & &a_{jn}\cr
              & & & &*& &\cr
              & & & & & & & \cr
          }
          =
          \bordermatrix{
              & & & & & \cr 
              & & & &*& \cr
             i&a_{i1}+ \lambda a_{j1} & & &\hdots & & &a_{in} + \lambda
             a_{jn}\cr
              & & & &*& \cr
              & & &\cr
             j&a_{j1} & & &\hdots & & &a_{jn}\cr
              & & & &*& &\cr
              & & & & & & & \cr
          }
          \)
          }
    \end{enumerate}
  \item Bei einer elementaren Umformung bleibt der Rang einer Matrix
    erhalten, da \(Rang(B A) = Rang(A)\), falls \(B\) invertierbar
    ist.
  \item Analog definiert man elementare Spaltenumformungen
  \item Elmentare Zeilenumformungen des LGS verändern die
    Lösungsmenge nicht (Satz 3.9)
\end{enumerate}
\subsubsection{Beispiel}

\begin{align*}
  \begin{cases}
    3 x_1  + 5 x_2 & = -2\\
    \! x_1    - 6 x_2 & = 7\\
  \end{cases}
  & \xLeftrightarrow{\text{Zeilen Tauschen}}
  \begin{cases}
    x_1 -6x_2 &= 7\\
    3x_1 + 5x_2 &= -2\\
  \end{cases}\\
  & \xLeftrightarrow{I - \frac{1}{3}II}
    \begin{cases}
      x_1 - 6 x_2 &= 7\\
      -\frac{23}{3} x_2 &= \frac{23}{3}\\
    \end{cases}\\
  & \xLeftrightarrow{-\frac{3}{23} II}
    \begin{cases}
      x_1 - 6 x_2 &= 7\\
      x_2 &= -1\\
    \end{cases}\\
  & \xLeftrightarrow{I + 6II}
    \begin{cases}
      x_1 &= 1\\
      x_2 &= 2\\
    \end{cases}\\
\end{align*}
\subsection{Zeilenstufenform}
\begin{enumerate}
\item Ein LGS ist in der Zeilenstufenform, falls die erweiterte
Koeffizientenmatrix die folgende Form hat:\\

\scalebox{.82}{$
   [A b] = \left[\begin{array}{cccccccccccccccc|l}
 0 & \hdots & 0 & \bigtriangleup & *     & \hdots & *     & *              & *      & \hdots & *     & \hdots  & *      & *      & \hdots & * & \beta_1 \\[3mm]
 0 & \hdots & 0 &    0           & \hdots & \hdots & 0     & \bigtriangleup & *      & \hdots & *     & \hdots  & *      & *      & \hdots & * & \beta_2 \\[3mm]
\vdots &   & \vdots & \vdots    &       &       &       &                &        &       &       &        & \vdots & \vdots &  & \vdots & \vdots  \\[3mm]
 0 & \hdots & \hdots  & \hdots     & \hdots & \hdots & \hdots & \hdots          & \hdots  & \hdots & \hdots & 0 & \bigtriangleup & *   & \hdots & * & \beta_r \\[3mm]
 0 & \hdots & \hdots  & \hdots     & \hdots & \hdots & \hdots & \hdots          & \hdots  & \hdots & \hdots & 0      & 0      & \hdots  & \hdots & 0 & \beta_{r+1} \\[3mm]
\vdots &   &        &           &       &       &       &                &        &       &       & \vdots & \vdots &        &  & \vdots & \vdots \\[3mm]
 0 & \hdots & \hdots  & \hdots     & \hdots & \hdots & \hdots & \hdots          & \hdots  & \hdots & \hdots & \hdots  & \hdots  & \hdots  & \hdots & 0 & \beta_m
   \end{array}\!\!\!\right]
$}\\
\(\bigtriangleup \neq 0\) heißen Pivotelemente, \(*\) sind beliebige
Elemente.
\item Gilt zusätzlich:
  \begin{enumerate}
    \item jedes Pivotelement \( = 1\)
    \item über jedem Pivotelement stehen nur Nullen
  \end{enumerate}
  so ist das LGS in der normierten Zeilenstufenform (NZSF)
\end{enumerate}
\(Rang(A) = Zeilenrang(A) = Spaltenrang(A) = r\).
\(r\) ist die Anzahl der Pivotelemente und damit die Anzahl der linear
unabhängigen Spalten.

\subsubsection{Bemerkungen}
\begin{enumerate}
\item \(\exists i \in \left\{r + 1, \hdots, n\right\}: \beta_i \neq 0\), 
  dann ist das Gleichungssystem nicht lösbar (\(Rang(A) < Rang([A b])\)).
\item Ist \(\beta_{r+1} = \hdots = \beta_m = 0\), dann ist das lineare
  Gleichungssystem lösbar. Wenn \(\underbrace{r}_{\text{ Anzahl
          Pivotelemente }} = n\), so hat das LGS genau eine Lösung.\\
      Wenn \(r < n\), so hat das LGS unendlich viele
      Lösungen.\\
      Falls in der i-ten Spalte kein Pivot-Element steht, so heißt die
      Variable \(x_i\) freie Variable. Es gibt \(n - r\) freie
      Variablen.
\end{enumerate}
\subsection{Algorithmen}
\subsubsection{Gauß-Algorithmus (Berechung der ZSF)}
Gegeben: \(A \in K^{m \times n}\). Berechne ZSF von \(A\).
\begin{enumerate}
\item Zeile:
  \begin{enumerate}
  \item 
    \begin{minipage}{0.4\textwidth}
      Suche eine Zeile \(i_1\), deren Eintrag \(\neq 0\) und
      den kleinsten Spaltenindex \(j_1\) hat. Tausche diese
      Zeile mit der ersten Zeile von \(A\)\\
      (Multiplikation von links mit der Permutationsmatrix
      \(P_{1 i_1}\).)
    \end{minipage}
    \begin{minipage}{0.4\textwidth}
      \[A = 
        \begin{bmatrix}
          0 & 0 & 2\\
          \boxed{2} & 4 & 6\\
          2 & -1 & 2\\
        \end{bmatrix}
        \underset{I \leftrightarrow II}{\rightarrow}
        \begin{bmatrix}
          \boxed{2} & 4 & 6\\
          0 & 0 & 2\\
          2 & -1 & 2\\
        \end{bmatrix}
      \]
    \end{minipage}
  \item 
    \begin{minipage}{0.4\textwidth}
      Elimination unter dem Pivot-Element:\\
      Addiere das \(\left(-\frac{a_{ij_1}}{a_{1j_1}}\right)\) -
      fache der 1. Zeile zur i-ten Zeile für \( i = 2, \hdots,
      m\).\\
      (Multiplikation von links mit
      \(G_{i1}\left(-\frac{a_{ij_1}}{a_{1j_1}}\right)\)).\\
    \end{minipage}
    \begin{minipage}{0.4\textwidth}
      \[
        \begin{bmatrix}
          2 & 4 & 6\\
          0 & 0 & 2\\
          2 & -1 & 2\\
        \end{bmatrix}
        \underset{(-1)I + III}{\rightarrow}
        \begin{bmatrix}
          2 & 4 & 6\\
          0 & 0 & 2\\
          0 & -5 & -4\\
        \end{bmatrix}
      \]
    \end{minipage}
  \end{enumerate}
\item [k.] Zeile (für \(k = 2,\hdots, m\)):
  Betrachte nur noch die Matrix \(\tilde{A}_K\) mit den Zeilen \(k,\hdots
  m\) und den Spalten \(j_{k-1} + 1, \hdots, n\).
  \begin{enumerate}
    \item 
      \begin{minipage}{0.4\textwidth}
        Suche eine Zeile von \(\tilde{A}_k\) (\(i_k\)-te Zeile von
        \(A_k\)) deren Eintrag von \(0\) verschieden ist und den
        kleinsten Spaltenindex hat (\(j_k\)-te Spalte von
        \(A_k\)). Tausche die gefundene \(i_k\)-te Zeile mit der k-ten
        Zeile von \(A_k\)\\
        \(\left(P_{ki_k}\right)\)
      \end{minipage}
      \begin{minipage}{0.4\textwidth}
        \[
         \begin{bmatrix}
          2 & 4 & 6\\
          0 & 0 & 2\\
          0 & \boxed{-5} & -4\\
        \end{bmatrix}
        \underset{II \leftrightarrow III}{\rightarrow}
         \begin{bmatrix}
          2 & 4 & 6\\
          0 & \boxed{-5} & -4\\
          0 & 0 & 2\\
        \end{bmatrix}
       \]
      \end{minipage}
    \item 
      \begin{minipage}{0.4\textwidth}
        Addiere das \(\left(-\frac{a_{ij_k}}{a_{kj_k}}\right)\)-fache
        der k-ten Zeile zur i-ten Zeile für \(i = k + 1, \hdots, m
        \).\\
        \(\left(G_{ik}\left(-\frac{a_{ij_k}}{a_{kj_k}}\right) \cdot
          A_k\right) \rightsquigarrow A_{K+1}\)
        \end{minipage}
  \end{enumerate}
  
\end{enumerate}
\subsubsection{Gauß-Algorithmus II (NZSF)}
Gegeben: \(A \in K^{m \times n}\) in ZSF.\\
Berechne NZSF von \(A\):
\begin{enumerate}
  \item Normierung der Pivotelemente:\\
    \begin{minipage}{0.4\textwidth}
      Dividiere die k-te Zeile durch ihr Pivotelement \(a_{kj_k}\)\\
      \(\left(\text{ Multiplikation von links mit } M_k
        \left(\frac{1}{a_{kj_k}}\right)\right)\)
    \end{minipage}
    \begin{minipage}{0.4\textwidth}
      \[
        \begin{bmatrix}
         \boxed{2} & 4 & 6\\
          0 & \boxed{-5} & -4\\
          0 & 0 & \boxed{2}\\
        \end{bmatrix}
        \underset{
            \begin{array}{c@{\cdot}c}
              \frac{1}{2}& I\\
              -\frac{1}{5}& II\\
              \frac{1}{2}& III\\
            \end{array}
            }{\rightarrow}
        \begin{bmatrix}
         \boxed{1} & 2 & 3\\
          0 & \boxed{1} & \frac{4}{5}\\
          0 & 0 & \boxed{1}\\
        \end{bmatrix}
      \] 
    \end{minipage}
  \item Elimination über dem Pivotelement:\\
    \begin{minipage}{0.4\textwidth}
      Addiere das \(\left(-a_{ki_k}\right)\)-fache der k-ten Zeile zur
      i-ten Zeile für \(\begin{subarray}{l} i = 1, \hdots, k-1\\ k = 2,
        \hdots, r\end{subarray}\)\\
      \(\left( G_{ik}(-a_{ij_k}) \cdot\right)\)
    \end{minipage}
    \begin{minipage}{0.4\textwidth}
      \[
        \begin{bmatrix}
          1 & \boxed{2} & 3\\
          0 & 1 & \frac{4}{5}\\
          0 & 0 & 1\\
        \end{bmatrix}
        \underset{I - 2 II}{\rightarrow}
        \begin{bmatrix}
          1 & \boxed{0} & \frac{7}{5}\\
          0 & 1 & \frac{4}{5}\\
          0 & 0 & 1\\
        \end{bmatrix}
        \underset{
            \begin{array}{c@{-}c@{\cdot}c}
              I &\frac{7}{5} & II\\
              II & \frac{4}{5} & III\\
            \end{array}
            }{\rightarrow}
        \begin{bmatrix}
          1 & 0 & 0\\
          0 & 1 & 0\\
          0 & 0 & 1\\
        \end{bmatrix}
      \]
    \end{minipage}
\end{enumerate}
\subsubsection{Bemerkung}
Sei \(A \in K^{m \times n}\). Dann gibt es \(p \in \mathbb{N}\) und
Elementarmatrizen \(S_1, \hdots, S_p \in K^{m \times m}\), sodass
\([\tilde{A}\tilde{b}] = S_p \cdot \hdots \cdot S_1 \cdot [Ab]\) in ZSF (bzw
NZSF) ist.

\subsubsection{Algorithmus (Lösung von LGS)}
Gegeben: \(A \in K^{m \times n}, b \in K^m \).\\
Bestimme \(L(A, b)\):
\begin{enumerate}
\item Bringe \([Ab]\) in die NZSF \([\tilde{A}\tilde{b}]\) mit \(\tilde{A} =
  [\tilde{a}_{ij}], \tilde{b} = [\tilde{b}_{ij}]\)
\item Bestimme die \((n -r)\) freien Variablen  \(x_j, j \in
  \underbrace{\left\{1,\hdots,n\right\}\backslash\left\{j_1,\hdots,j_r\right\}}_{II},
  r = Rang(A)\)
\item Bestimme die allgemeine Lösung:
  \[x_{j_k} = \tilde{b}_k - \sum\limits_{j\in II} \tilde{a}_{kj}x_j ,k=1,\hdots,r\]
\end{enumerate}

\subsubsection{Beispiel}
\(
  \begin{array}{c r c r c r c r c r@{=}r}
     &    & & 2x_2 &+& 6x_3 &-& 2x_4 &-& 4x_5 &  4\\
    -& x_1&+& 2x_2 &+& 8x_3 & &      &-&  x_5 &- 3\\
     &3x_1&-& 5x_2 &-&21x_3 &-&  x_4 &+&  x_5 & 11\\
     & x_1&-& 3x_2 &-&11x_3 &+& 3x_4 &+& 3x_5 &- 1\\
  \end{array}
\)
\[
\begin{array}{c c c}
  \left[
    \begin{array}{c c c c c|c}
        0 & 2 &  6 &-2 & -4 &  4\\
      \boxed{- 1} & 2 &  8 & 0 & -1 &- 3\\
        3 &-5 &-21 &-1 &  1 & 11\\
        1 &-3 &-11 & 3 &  3 &- 1\\ 
    \end{array}
  \right]
  &\underset{I \leftrightarrow II}{\rightarrow}&
  \left[
    \begin{array}{c c c c c|c}
      - 1 & 2 &  8 & 0 & -1 &- 3\\
        0 & 2 &  6 &-2 & -4 &  4\\
        3 &-5 &-21 &-1 &  1 & 11\\
        1 &-3 &-11 & 3 &  3 &- 1\\ 
    \end{array}
  \right]\\
  &\underset{\begin{array}{c@{+}c}
               3I & III\\
               I&IV\\
             \end{array}}{\rightarrow}&
  \left[
    \begin{array}{c c c c c|c}
      \boxed{- 1} & 2 &  8 & 0 & -1 &- 3\\
        0 &\boxed{2}&  6 &-2 & -4 &  4\\
        0 & 1 &  3 &-1 & -2 &  2\\
        0 &-1 &- 3 & 3 &  2 &- 4\\ 
    \end{array}
  \right]\\
  &\underset{\begin{array}{c@{+}c}
               -\frac{1}{2}II&III\\
                \frac{1}{2}II&IV\\
             \end{array}}{\rightarrow}&
  \left[
    \begin{array}{c c c c c|c}
      - 1 & 2 &  8 & 0 & -1 &- 3\\
        0 & 2 &  6 &-2 & -4 &  4\\
        0 & 0 &  0 & 0 &  0 &  0\\
        0 & 0 &  0 & \boxed{2} &  0 &- 2\\ 
    \end{array}
  \right]\\
  &\underset{III \leftrightarrow IV}{\rightarrow}&
  \left[
   \begin{array}{c c c c c|c}
     - 1 & 2 &  8 & 0 & -1 &- 3\\
       0 & 2 &  6 &-2 & -4 &  4\\
       0 & 0 &  0 & \boxed{2} &  0 &- 2\\ 
       0 & 0 &  0 & 0 &  0 &  0\\
   \end{array}
  \right] (ZSF)\\                                               
\end{array}
\]
\[
  \begin{subarray}{c}
    Rang(A) = 3 = Rang([A b])\\
    r = 3, m = 5\\
    3 < 5 \Rightarrow \text{Mehrere Lösungen}\\
  \end{subarray}
\]
\(\begin{array}{l r}
    j_1 = 1 & II = \{3,5\}\\
    j_2 = 2 & x_3, x_5 \text{ - freie Variablen}\\
    j_3 = 4
  \end{array}
\)
\[
\begin{array}{c c c}
  \left[
   \begin{array}{c c c c c|c}
     - 1 & 2 &  8 & 0 & -1 &- 3\\
       0 & 2 &  6 &-2 & -4 &  4\\
       0 & 0 &  0 & 2 &  0 &- 2\\ 
       0 & 0 &  0 & 0 &  0 &  0\\
   \end{array}
  \right]
  &\underset{\begin{array}{c@{\cdot}c}
                        -1&I\\
               \frac{1}{2}&II\\
               \frac{1}{2}&III\\               
             \end{array}}{\rightarrow}&

  \left[
   \begin{array}{c c c c c|c}
     1 &-2 & -8 & 0 &  1 &  3\\
     0 & 1 &  3 &-1 & -2 &  2\\
     0 & 0 &  0 & 1 &  0 &- 1\\ 
     0 & 0 &  0 & 0 &  0 &  0\\
   \end{array}
  \right]\\
  &\underset{\begin{array}{c@{+}c}
                        I&2II\\         
             \end{array}}{\rightarrow}&

  \left[
   \begin{array}{c c c c c|c}
     1 & 0 & -2 &-2 & -3 &  7\\
     0 & 1 &  3 &-1 & -2 &  2\\
     0 & 0 &  0 & 1 &  0 &- 1\\ 
     0 & 0 &  0 & 0 &  0 &  0\\
   \end{array}
  \right]\\
  &\underset{\begin{array}{c@{+}c}
                I&2III\\
               II&III\\
             \end{array}}{\rightarrow}&

  \left[
   \begin{array}{c c c c c|c}
     1 & 0 & -2 & 0 & -3 &  5\\
     0 & 1 &  3 & 0 & -2 &  1\\
     0 & 0 &  0 & 1 &  0 &- 1\\ 
     0 & 0 &  0 & 0 &  0 &  0\\
   \end{array}
  \right]\\
\end{array}
\]
\[
  \begin{array}{c@{=} r c r c r}
    x_1 & 5 & + & 2x_2 & + & 3x_5\\
    x_2 & 1 & - & 3x_3 & + & 2x_5\\
    x_4 &-1
  \end{array}
\]
\begin{align*}
  L(A,b) = 
   \left\{
    \begin{bmatrix}
     5&+&2x_3 & +& 3x_5\\
     1&-&3x_3 & +& 2x_5\\
      & & x_3 &  & \\
    -1& &     &  & \\
      & &     &  & x_5\\
    \end{bmatrix}
  :x_3, x_5 \in \mathbb{R}\right\}
  &=
  \begin{bmatrix}
  5\\
  1\\
  0\\
  -1\\
  0\\
  \end{bmatrix}
  + 
  \left\{
   \begin{bmatrix}
    2x_3 & + & 3x_5\\
   -3x_3 & + & 2x_5\\
     x_3 &&\\
     0 &&\\
     &&x_5\\
   \end{bmatrix}
  :x_3, x_5 \in \mathbb{R}\right\}
  = \\
  \begin{bmatrix}
  5\\
  1\\
  0\\
  -1\\
  0\\
  \end{bmatrix}
  + 
  \left\{
   x_3
   \begin{bmatrix}
    2\\
   -3\\
    1\\
    0\\
    0\\
  \end{bmatrix}
   +
  x_5
  \begin{bmatrix}
  3\\
  2\\
  0\\
  0\\
  1\\
  \end{bmatrix}
  :x_3, x_5 \in \mathbb{R}\right\}
  &= x + L(A,0)\\
\end{align*}
\subsubsection{Weitere Möglichkeiten}
\paragraph*{Frage:}
Was kann man noch mit dem Gauß-Algorithmus berechnen ?
\paragraph{Basis von \(Kern(A)\)} 
\(Av_i = 0, i = 1, \hdots, n-r\) mit \(r = Rang(A)\)
\begin{enumerate}
  \item Bringe \(A\) in NZSF \(\tilde{A} = [\tilde{a}_{ij}]\) und
    bestimme \(r = Rang(A)\)
  \item Bestimme die \((n-r)\) freien Variablen \(x_j, j\in II =
    \left\{1, \hdots, n \right\}\backslash \left\{j_1, \hdots,
      j_r\right\} = \left\{j_{r+1},\hdots, j_{n}\right\}\)
  \item Konstruiere die Basis \(v_1, \hdots, v_{n-r}\) von \(Kern(A)\)
    mit:\\
    \[v_k = \sum\limits_{s = i}^{min(r,j_{r
  +k}-1)}\left(-\tilde{a}_{sj_{r+k}}\right)e_{j_s} + e_{j_{r+k}},
  k = 1, \hdots, n-r\]
\subparagraph*{Zum Beispiel:}
\( 
 v_1 = 
  \begin{bmatrix}
    2\\-3\\1\\0\\0\\
  \end{bmatrix},
 v_2 = 
  \begin{bmatrix}
    3\\2\\0\\0\\1\\
  \end{bmatrix}
\)
\end{enumerate}
\paragraph{Basis von Bild \(Bild(A)\)}
\( = Span (a_1, \hdots, a_n)\)  
\subparagraph*{Beobachtungen}
\begin{enumerate}
  \item Ist \(S\) invertierbar, dann gilt: \(Bild(A) = Bild(AS)\)\\
    {\bf Beweis:}
    \begin{itemize}
      \item[\(\subseteq\)]:
        Sei \(y \in Bild(A) \Rightarrow \exists x: y = Ax = AIx = A
        (SS^{-1})x \Rightarrow y\in Bild(AS)\)
      \item[\(\supseteq\)]:
        Sei \(z \in Bild(AS) \Rightarrow \exists v: z = A(Sv)
        \Rightarrow z \in Bild(A)\)
    \end{itemize}
  \item Linear unabhängige Spalten von A bilden eine Basis von
    \(Bild(A)\).\\
    \(\Rightarrow\) transponierte linear unabhängige Zeilen von
    \(A^T\) bilden eine Basis von \(Bild(A)\):
    \[ A = \begin{bmatrix}
             \boxed{\verbar{1cm}}& 
             \verbar{1cm}& 
             \boxed{\verbar{1cm}}& 
             \verbar{1cm}\\
           \end{bmatrix},
      A^T=\begin{bmatrix}
             \boxed{\horbar{1cm}}\\
             \horbar{1cm}\\
             \boxed{\horbar{1cm}}\\
             \horbar{1cm}\\ 
           \end{bmatrix}
         \]
\end{enumerate}
\subparagraph*{Algorithmus}
\begin{enumerate}
  \item Bringe \(A^T\) in NZSF.\\
    \(\tilde{A}^T = S \cdot A^T \left(\left(A^T\right)^T = \tilde{A} =
      A\cdot S^{T}\right)\)\footnote{\(S\): Multiplizierte
        Transformationen der Matrix (Einheitsmatrizen), daher
        Invertierbar}.
    \item Die ersten \(r\) Spalten von \(\tilde{A}\) bilden eine Basis
      von \(Bild(A)\).
\subparagraph*{Beispiel}
Sei 
\(A = 
 \begin{bmatrix}
   1 & -1 & -1\\
   -2& 5 & 3\\
 \end{bmatrix}
\)
\[
\begin{array}{r r r r l}
  A^T &=& 
   \begin{bmatrix}
    1 & -2\\
   -1 & 5\\
    1 & 3\\
   \end{bmatrix}
  &\underset{\begin{array}{c@{+}c}I&II\\I&III\\\end{array}}{\rightarrow}& 
   \begin{bmatrix}
   \boxed{1} & -2\\
    0 & \boxed{1}\\
    0 & 1\\
   \end{bmatrix}\\
  &&&\underset{\begin{array}{c@{+}c}\frac{1}{3}II&III\\\end{array}}{\rightarrow}& 
   \begin{bmatrix}
    1 & -2\\
    0 & 1\\
    0 & 0\\
   \end{bmatrix} ZSF \\
  &\Rightarrow &\left\{v_1 = \begin{bmatrix}1\\-2\\\end{bmatrix}, v_2 = \begin{bmatrix}0\\3\\\end{bmatrix}\right\}
\end{array}
\]
\end{enumerate}
\paragraph{Inverse von A}
\subparagraph*{Beobachtungen:}
\begin{enumerate}
\item Sei \(A\in K^{n\times n}\) und seien \(S_1, \hdots, S_p \in
  K^{n\times n}\) Elementarmatrizen, so dass \(\tilde{A} = S_p \cdot
      \hdots \cdot S_1 \cdot A \) in NZSF ist.\\
      Dann gilt: 
      \[A \text{ ist invertierbar }\Leftrightarrow \tilde{A} = I_n\]
      {\bf Beweis:}
      \begin{itemize}
        \item[\(\Leftarrow\)] trivial, da \(A = S_1^{-1}\cdot \cdots
          \cdot S_p^{-¹}\) als Produkt invertierbarer Matrizen
          invertierbar ist. \(I = S_p\cdot \hdots \cdot S_1 \cdot A,
          S_p^{-1} = S_{p-1} \cdot \hdots \cdot S_1 \)
        \item[\(\Rightarrow\)] \(A, S_1, \hdots, S_p\) invertierbar
          \(\Rightarrow \tilde{A}\) invertierbar.\\
          Seien \(\tilde{a}_{1j_1}, \hdots, \tilde{a}_{rj_r}\) mit
          \(r\leq n\)\\
          \begin{align*}
            \text{Angenommen: } r < n &\Rightarrow \tilde{A} \text{ hat
                                        eine Nullzeile
                                        }\lightning_{\text{ zur
                                        Invertierbarkeit von
                                        }\tilde{A} \text{(ÜB10,HA)}}\\
            \Rightarrow r = n&
          \end{align*}
      \end{itemize}
\item Ist \(A\) invertierbar und \(S_p \cdot \hdots \cdot S_1 \cdot A\)
  in NZSF\\
  \(\Rightarrow S_p\cdot \hdots \cdot S_1 \cdot A = I \Rightarrow
  A^{-1} = S_p \cdot\cdots \cdot S_1 \cdot I\).
  \[[A|I] = [I|A^{-1}]\]
\end{enumerate}
\subparagraph*{Algorithmus}
\begin{enumerate}
  \item Bringe die erweiterte Matrix \([A|I]\) in NZSF
    \([\tilde{A}|S]\)
  \item Ist \(\tilde{A}\) nicht die \(I_n\), so ist \(A\) nicht
    invertierbar, andernfalls: \(A^{-1} = S\).
\end{enumerate}
\subparagraph*{Beispiel}
Sei 
\(A = 
\begin{bmatrix}
  1 & 2\\
  3 & 5\\
\end{bmatrix}
\)
\begin{align*}
  \left[\begin{array}{c c|c c}
    1 & 2 & 1 & 0\\
    3 & 5 & 0 & 1\\
  \end{array}\right]
  & \underset{-3\cdot I + II}{\rightarrow} 
  \left[\begin{array}{c c|c c}
    1 & 2 & 1 & 0\\
    0 & -1& 3&1\\
  \end{array}\right]\\
  & \underset{ -II}{\rightarrow} 
  \left[\begin{array}{c c|c c}
    1 & 2 & 1 & 0\\
    0 & 1& -3&-1\\
  \end{array}\right]\\
  & \underset{ (-2)II +I}{\rightarrow}
  \left[\begin{array}{c c|c c}
    1 & 0& -5 & 2\\
    0 & 1&  3&-1\\
  \end{array}\right]\\
  \Rightarrow A^{-1} = \begin{bmatrix}-5&2\\3&-1\\\end{bmatrix}&
\end{align*}
\paragraph{Darstellung eines Vektors \(b\) als Linearkombination von
    \(a_1,\hdots, a_n\)}
\[b = x_1a_1 + \hdots + x_n a_n = 
  \begin{bmatrix}a_1& \hdots& a_n\end{bmatrix}
  \begin{bmatrix}x_1\\\vdots\\x_n\end{bmatrix}
  = Ax
\]
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Lineare_Algebra_1"
%%% End:
