\section{Mengen}
\subsection{Menge}
\subsubsection{Definition}
Nach G. Cantor (1895):
\begin{quote}
Unter einer Menge verstehen wir jede Zusammenfassung \(M\), von
bestimmten, wohl unterschiedenen Objekten \(m\) unserer Anschauung
oder unseres Denkens zu einem Ganzen.
\end{quote}
Seien \(M, N\) Mengen, gilt also:
\begin{center}
  \begin{tabular}{c@{ "= }l}
    \(m \in M\) & \(m\) ist ein Element von (gehört zur Menge) \(M\)\\
    \(M=N\)     & \(m \in M \Leftrightarrow m\in N\)\\
    \(m \notin M\)& \(m\) ist nicht Element von \(M\) (\(\lnot (m\in M)\))\\
  \end{tabular}
\end{center}
Mengen werden definiert durch eine der folgenden Methoden:
\begin{itemize}
  \item Durch die Aufzählung der in der Menge enthaltenen Elemente
  \item Durch eine characteristische Eigenschaft:\\
    Sei \(A(x)\) die characteristische Eigenschaft aller Elemente in
    \(M\).\\
    So schreibt man:
    \[M=\left\{ x \big| A(x) \right\} \]
    Der senkrechte Strich \(|\) besagt, dass \(x\) die Eigenschaft
    \(A(x)\) besitzt.\\
    Man spricht:
     \glqq \(x\) mit der Eigenschaft \(A(x)\)\grqq
\end{itemize}

\subsubsection{Beispiele}
\begin{enumerate}
  \item \(N = \{1,2,3\} = \{2,3,1\} = \{1,2,1,3\}\):\\
    Die Reihenfolge der Elemente in Mengen ist irrelevant für die
    Gleichheit. Genauso sind redundante Elemente, da per Definition das
    Element nur enthalten sein muss, irrelevant. 
  \item \(\mathbb{N} := \{1,2,3,4,5,\dots\}\):\\
    Die Definition der Menge der natürlichen Zahlen.
    \footnote{
    Das Symbol \(:=\) bedeutet: \glqq Ist per Definition.\grqq\ 
    So gilt hier: \(\mathbb{N}\) ist per Definition gleich \(\{1,2,3,\dots\}\).
    }
  \item \(\mathbb{N}_0 := \{0,1,2,3,4,\dots\}\):\\
    Die Definition der Menge der natürlichen Zahlen mit 0.
  \item \(\mathbb{Z} := \{0,\pm 1, \pm 2, \pm 3, \pm 4, \dots\}\):\\
    Die Definition der Menge der ganzen Zahlen.
  \item \(\mathbb{Q} := \left\{ \frac{p}{q} \big| p\in \mathbb{Z},
      q\in \mathbb{N} \right\}\):\\
    Die Definition der Menge der rationalen Zahlen.
  \item \(\mathbb{R} := \{x|x \text{ ist eine reelle Zahl}\}\):\\
    Menge der reellen Zahlen
  \item \(U := \{1,3,5,7,9,11,\dots\} = \{m\in \mathbb{N}|\exists k
    \in\mathbb{N}_0 \text{ mit } n = 2\cdot k +1\} = \{2\cdot k + 1|k\in
    \mathbb{N}_0\} \):\\
    Menge der ungeraden Zahlen.
\end{enumerate}

\subsection{Teilmengen}
\subsubsection{Definition}
Seien \(M, N\) Mengen.
\begin{itemize}
  \item \(N\) heißt Teilmenge von \(M\) wenn für jedes \(x\) gilt
    \(x\in N \Rightarrow x \in M\)
    \[N \subseteq M \Leftrightarrow \forall x (x \in N \Rightarrow x\in M)\]
  \item \(N\) echte Teilmenge von \(M\) wenn für jedes \(x\) gilt:
    \(x\in N \Rightarrow x\in M\) und \(N \neq M\)
    \footnote{Statt \(\subset\) wird auch \(\subsetneq\) verwendet. \(\subset\) wird
        in mancher Literatur äquivalent zu \(\subseteq\) verwendet.}
    \[N \subset M \Leftrightarrow N \subseteq M \land N \neq M\]
\end{itemize}

\subsubsection{Bemerkungen}
\begin{enumerate}
  \item \(M \subseteq M\)
  \item Ist \(\varnothing\) eine leere Menge, so gilt 
    \(\varnothing \subseteq M\):
    \begin{itemize}
      \item \(x \in \varnothing\) ist immer falsch
      \item \(x \in \varnothing \Rightarrow x\in M\) ist immer wahr
    \end{itemize}
  \item \(M = N \Leftrightarrow (M \subseteq N \land N \subseteq M)\)
  \item Es gibt genau eine leere Menge.\\
    Beweis:
    \begin{itemize}
      \item {\bf Existenz:}
        \[\{x|x \neq x\}\]
      \item {\bf Eindeutigkeit:}
        Seien \(\varnothing, \varnothing'\) leere Mengen
        \begin{align*}
          &\Rightarrow \varnothing \subseteq \varnothing' 
            \land \varnothing' \subseteq \varnothing\\
          &\Rightarrow \varnothing = \varnothing'
        \end{align*}    
    \end{itemize}
  \item \(\mathbb{N} \subset \mathbb{Z} \subset \mathbb{Q} 
           \subset \mathbb{R} \subset \mathbb{C}\).
\end{enumerate}

\subsection{Verknüpfungen von Mengen}
\subsubsection{Definitionen}
Seien \(N, M\) Mengen.
\begin{enumerate}
  \item Schnittmenge:\\
    Sie gibt den \glqq\ Durchschnitt \grqq\ der Mengen \(N\) und \(M\)
    an.
    \[N \cap M := \{x|x\in M \land x\in N\}\]
    {\centering\includegraphics[width=4cm]{img/Schnittmenge.pdf}\\}
  \item Vereinigungsmenge:\\
    Sie gibt die Vereinigung der Mengen \(N\) und \(M\) an. 
    \[N \cup M := \{x|x\in M \lor x \in N\}\]
    {\centering\includegraphics[width=4cm]{img/Vereinigungsmenge.pdf}\\}
  \item Differenzmenge:\\
    Sie gibt die Differenz der Mengen \(M\) und \(N\) an:\\
    \[N \setminus M := \{x|x\in N \land x\notin M\}\]
    {\centering\includegraphics[width=4cm]{img/Differenzmenge.pdf}\\}
  \item Kartesisches Produkt:\\
    Das kartesische Produkt von Mengen gibt die Menge der geordneten
    Paare an, die in den Mengen existieren.
    \[M\times N := \{(x,y)|x\in M \land y \in N\}\]
    {\centering\includegraphics[width=6cm]{img/Kartesisches_Produkt.pdf}\\}
    \begin{itemize}
      \item Für Mengen \(M, \dots M_k\) sei:
        \[M_1 \times M_2 \times \dots \times M_k =\{(x_1, \dots ,x_k) | x_i \in
          M_i, i = 1, \dots, k\}\]
      \item \(M^k = \underbrace{M \times\dots\times M}_{\text{k-mal}}\) 
    \end{itemize}
    Beispiel:\\
    Seien \(M = \{1,2\}, N = \{a, b, c\}\) Mengen.\\
    \(M \times N = \left\{ (1,a),(1,b), (1,c), (2,a), (2,b), (2,c)\right\} \)
\end{enumerate}

\subsubsection{Bemerkung}
\begin{enumerate}
  \item Seien \(M_1, \dots ,M_k\) Mengen:
    \begin{align*}
      \bigcap\limits^k_{i=1} M&= M_1\cap M_2 \cap \dots \cap M_k\\
      \bigcup\limits^k_{i=1} M&= M_1\cup M_2 \cup \dots \cup M_k
    \end{align*}
  \item Sei \(\mathcal{M}\) eine Menge von Mengen:
    \begin{itemize}
    \item Schnittmenge:
      \[
        \bigcap\limits_{M \in \mathcal{M}}M := 
        \left\{x \big| \forall M \in \mathcal{M}: x\in M \right\}
      \]
    \item Vereinigung:
      \[
        \bigcup\limits_{M \in \mathcal{M}}M := 
        \left\{ x \big|\exists M \in \mathcal{M}: x\in M \right\}
      \]
\end{itemize}
Beispiel:\\
Sei \(M_n = \{ -n, 0, n\}\) für \(n \in \mathbb{N}\).\\
Sei \(\mathcal{M} = \{M_n|n \in \mathbb{N}\}\).
\begin{itemize}
  \item Schnittmenge:
    \[\bigcap\limits_{n\in\mathbb{N}}M_n =
      \left\{x \big| x\in \{-n, 0, n\}, n \in \mathbb{N} \right\} = \{0\}\]
    \(0\) ist als einziges Objekt in allen Mengen enthalten, daher als
    Einziges in der Schnittmenge dieser Objekte.
  \item Vereinigung:\\
    \[\bigcup\limits_{n\in\mathbb{N}}M_n=
      \left\{ x \big| x \in \{-n, 0, n\}, n\in \mathbb{N} \right\} = \mathbb{Z}\]
  \end{itemize}
\end{enumerate}

\subsection{Potenzmenge}
\subsubsection{Definition}
Sei \(M\) eine Menge.\\
Die Potenzmenge \(\mathcal{P}(M)\) ist die Menge aller Teilmengen von
\(M\):
\[\mathcal{P}(M):= \{N|N\subseteq M\}\]

\subsubsection{Beispiel}
Sei \(M = \{1,2,3\}\)\\
\(\mathcal{P}(M) = \{\varnothing, \{1\}, \{2\}, \{3\}, \{1,2\},
\{2,3\}, \{1,3\}, \{1,2,3\}\}\)

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Lineare_Algebra_1"
%%% End:
