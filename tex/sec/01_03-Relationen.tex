\section{Relationen}
\subsection{Relation}
\subsubsection{Definition}
Seien \(X, Y\) Mengen.
\begin{enumerate}
  \item Eine Teilmenge \(R \subseteq X \times Y\) heißt \emph{Relation} zwischen
    \(X\) und \(Y\).\\
    Ist \(X = Y\), so heißt \(R\subseteq X \times X\) \glqq Relation
    auf \(X\)\grqq .
  \item Für \((x,y)\in X \times Y\) sei 
    \(x R y \Leftrightarrow (x,y) \in R\)
  \end{enumerate}

\subsubsection{Beispiel}
\begin{enumerate}
  \item Sei \(X\) die Menge der Hörer dieser Vorlesung.\\
    \(R\subseteq X\times X\) sei definiert durch 
    \((x,y)\in R \Leftrightarrow x \text{ kennt } y\)
  \item Sei \(x \in R\).
    \[R \subseteq \mathbb{R} \times \mathbb{R} \Leftrightarrow x \leq y \]
\end{enumerate}

\subsection{Relationseigenschaften}
\subsubsection{Definition}
Eine Relation \(R\) auf eine Menge \(X\) heißt:
\begin{enumerate}
  \item reflexiv, 
    falls \(\forall x\in X: (x,x) \in R\)
  \item symmetrisch, 
    falls \(\forall x, y\in X:((x,y)\in R\Rightarrow (y,x)\in R )\)
  \item transitiv, 
    falls \(\forall x, y, z \in X:((x,y)\in R \land (y,z) \in R \Rightarrow (x,z) \in R)\) 
  \item antisymmetrisch, 
    falls \(\forall x, y\in X:((x,y)\in R \land (y,x) \in R \Rightarrow x=y)\)
  \item Äquivalenzrelation, falls \(R\) reflexiv, symmetrisch und transitiv ist.
\end{enumerate}

\subsubsection{Beispiele}
\begin{enumerate}
  \item Sei \(X\) die Menge aller Studenten an der Uni Augsburg.\\
    So ist 
    \(R:=\{(x,y)| x \text{ ist im gleichen Fachsemester wie } y\}\) 
    eine Äquivalenzrelation.
  \item 
    Sei \(X = \mathbb{Z}\). \\
    \(R := \{(x,y)\in \mathbb{Z}\times \mathbb{Z}| x \text{ teilt } y\} =
    \{(x,y)\in\mathbb{Z}^2 |\exists a \in \mathbb{Z}:y = x\cdot a\}\)
    \begin{itemize}
      \item {\bf Reflexivität:}
        Sei \(x\in \mathbb{Z}\) beliebig:\\
        \(x \cdot 1 = x \Rightarrow (x,x)\in R\).\\
        Somit ist \(R\) reflexiv. 
      \item {\bf Transitivität:}
        Seien \(x,y,z \in \mathbb{Z}, (x,y)\in R \land (y,z) \in R\).
        \begin{align*}
          &\Rightarrow \exists a,b\in \mathbb{Z} : y=x \cdot a, z = y\cdot b\\
          &\Rightarrow x \cdot (a\cdot b) = (x\cdot a) \cdot b = y\cdot b = z\\
          & \Rightarrow (x,z)\in R
        \end{align*}
        Somit ist \(R\) transistiv.
      \item {\bf Symmetrie:}
        Da \((2,4)\in R\land (4,2)\notin R\) gilt, ist \(R\) nicht
        symmetrisch.
      \item {\bf Antisymmetrie:}
        Da \((2,-2) \in R\land (-2, 2) \in R\land 2 \neq -2\) gilt, ist \(R\) nicht antisymmetrisch.
    \end{itemize}
  \item Sei \(R:= \left\{(x,y) \in \mathbb{R}^2 \big| x\leq y\right\}\).\\
    \(R\) ist antisymmetrisch, da gilt:
    \(\forall x,y \in R: (x,y) \in \mathbb{R} \land (y,x)\in R \Rightarrow x\leq y \land y \leq x \Rightarrow x=y\)
\end{enumerate}

\subsection{Äquivalenzklassen}
\subsubsection{Definition}
Sei \(X\) eine Menge und \(R\) eine Äquivalenzrelation auf \(X\).\\
Für \(x \in X\) heißt \([x]_R := \{y\in X |(x,y)\in R\}\) die
\emph{Äquivalenzklasse} von \(x\) bezüglich \(R\).\\
Jedes \(y \in [x]_R\) heißt \emph{Repräsentant} von \([x]_R\). 
\footnote{Jede Äquivalenzklasse ist das Bild eines
    Repräsentanten dieser Äquivalenzklasse bezüglich der Relation.}

\subsubsection{Bemerkung}
\begin{enumerate}
  \item \([x]\) statt \([x]_R\)
  \item \(x \in [x]_R\), da \((x,x)\in R\) (Reflexivität)
\end{enumerate}

\subsubsection{Satz}
Sei \(R\) eine Äquivalenzrelation auf \(X\) 
und seien \( x,y \in X\). 
Dann sind folgende Aussagen äquivalent:
\begin{enumerate}
  \item \((x,y) \in R\)
  \item \([x] = [y]\) 
  \item \([x]\cap [y] \neq \varnothing \) 
\end{enumerate}

\subsubsection{Beweis}
\begin{itemize}
  \item \(1 \Rightarrow 2\):
    Sei \(z \in [x]\)
    \begin{align*}
      &\Rightarrow (x,z) \in R, (x,y)\in R\\
      &\Rightarrow (x,z) \in R, (y,x)\in R\\
      &\Rightarrow (y,z) \in R \\
      &\Rightarrow z \in [y] \\
      &\Rightarrow [x] \subseteq [y]
    \end{align*}
    Analog dazu gilt:
    \[[y] \subseteq [x] \Rightarrow [x] = [y]\]
    \textqed
  \item \(2\Rightarrow 3\):
    \[x \in [x] = [y] \Rightarrow [x] \cap [y] \neq \varnothing\]
    \textqed
  \item \(3 \Rightarrow 1\):
    Sei \(z \in [x] \cap [y]\)
    \begin{align*}
      &\Rightarrow z\in[x] \land z\in [y]\\
      &\Rightarrow (x,z)\in R \land (y,z) \in R\\
      &\Rightarrow (x,z)\in R \land (z,y) \in R\\
      &\Rightarrow (x,y)\in R
    \end{align*}
    \textqed
\end{itemize}

\subsubsection{Satz}
Sei \(R\) eine Äquivalenzrelation auf \(X\). Die Menge der
Äquivalenzklassen bilden eine Zerlegung von \(X\) in dem Sinn, dass
jedes \(x \in X\) in genau einer Äquivalenzklasse enthalten ist und dass
zwei Äquivalenzklassen disjunkt sind.

\subsubsection{Beweis (durch Kontraposition)}
Sei \(x \in X \Rightarrow x \in [x]\), also gehört jedes \(x\) zu
einer Äquivalenzklasse.\\
Seien \(x, y \in X\)
\begin{align*}
\text{Falls }& [x] \cap [y] \neq \varnothing\\
\Rightarrow & [x] = [y] 
\end{align*}
\textqed

\subsubsection{Menge aller Äquivalenzklassen}
Die Menge aller Äquivalenzklassen auf \(X\) schreibt man als \(X/R\)

\subsubsection{Beispiel}
Seien \(X = \mathbb{Z}, R := \{(x,y)\in \mathbb{Z}^2|x-y \text{ gerade}\}\)
\begin{itemize}
  \item {\bf Reflexivität:}
    Sei \(x \in \mathbb{Z}\):
    \begin{align*}
      \Rightarrow& x-x = 0 \text{ gerade}\\
      \Rightarrow& (x,x)\in R
    \end{align*}
    \textqed
  \item {\bf Symmetrie:}
    Seien \(x, y \in \mathbb{Z}, (x,y) \in R\):
    \begin{align*}
      \Rightarrow& x-y \text{ gerade}\\
      \Rightarrow& y-x = -(x-y) \text{ gerade}\\
      \Rightarrow& (y,x)\in R
    \end{align*}
    \textqed
  \item {\bf Transitivität:}
    Seien \(x, y, z \in \mathbb{Z}, (x,y), (y, z) \in R\):
    \begin{align*}
      &\Rightarrow x-z = (x-y)+y-z \text{ gerade}\\
      &\Rightarrow (x,z) \in R
    \end{align*}
    \textqed
\end{itemize}

\(\Rightarrow\) Äquivalenzklassen können eingeführt werden.\\
\(R\) ist also eine Äquivalenzrelation auf \(X\).\\
\[\mathbb{Z}/R = \{\{2\cdot k|k\in \mathbb{Z}\},\{2\cdot k + 1|k\in
  \mathbb{Z}\}\}\]
Menge der Äquivalenzklassen in \(\mathbb{Z}\) für \(R\). 



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Lineare_Algebra_1"
%%% End:
