\section{Eigenschaften linearer Abbildungen}
\subsection{Wiederholung}
Ist \(f: V \rightarrow W\) eine lineare Abbildung so gilt:
\begin{align*}
  \forall v,w \in V: f(v + w)&= f(v) + f(w)\\
  \forall v,w \in V, \lambda \in K: f(\lambda v)
  &= \lambda f(v)
\end{align*}
Eine lineare Abbildung heißt auch \emph{Vektorraumhomomorphismus}.\\
Ist \(f \in \mathscr{L}(V, W)\), so ist \(f\) eine lineare Abbildung,
da \(\mathscr{L}(V,W)\) die Menge aller linearen Abbildungen von 
\(V \rightarrow W\) ist.\\
Kern und Bild der Matrix sind definiert wie folgt:
\begin{align*}
  Kern(f) &:= \left\{ v\in V\mid f(v) = 0\right\} \subseteq V\\
  Bild(f) &:= \left\{ w\in W\mid \exists v \in V: f(v) = w\right\}
            \subseteq W
\end{align*}
Daraus lässt sich die Dimensionsformel ableiten:
\[dim(V) = dim(Kern(f)) + dim(Bild(f))\]

\subsection{Eigenschaften linearer Abbildungen}
\subsubsection{Definition: Rang einer Abbildung}
Sei \(f \in \mathscr{L}(V,W)\).\\
Der Rang einer Abbildung ist defniert als 
\[Rang(f) := dim(Bild(f))\]

\subsubsection{Definition: Eigenschaften}
Eine lineare Abbildung \(f \in \mathscr{L}(V,W)\) heißt:
\begin{enumerate}
\item Monomorphismus, wenn \(f\) injektiv ist 
  (\(\Leftrightarrow Kern(f) = \left\{0\right\}\))
\item Epimorphismus, wenn \(f\) surjektiv ist 
  (\(\Leftrightarrow Bild(f) = W\))
\item Isomorphismus, wenn \(f\) bijektiv ist
\item Endomorphismus, wenn \(W = V\) 
\item Automorphismus, wenn \(W = V\) und \(f\) bijektiv
\end{enumerate}

\subsubsection{Satz (4.1)}
Seien \(\left\{v_1, \hdots, v_n\right\}\) eine Basis von \(V\) und
\(w_1, \hdots, w_n  \in W\).\\
Dann gibt es genau eine lineare Abbildung \(f \in \mathscr{L}(V,W)\)
mit \(f(v_i) = w_i, i = 1, \hdots, n\)

\subsubsection{Beweis}
Sei \(v\in V \Rightarrow v = \sum\limits_{i=1}^n \lambda_i v_i\) mit
\(\lambda_1, \hdots, \lambda_n \in K\).\\
\begin{itemize}
  \item {\bf Existenz:} Übung 7, PA 4
  \item {\bf Eindeutigkeit:} Seien \(f, g : V\rightarrow W\) lineare
    Abbildungen mit \(f(v_i) = w_i = g(v_i)\) mit \(i = 1, \hdots, n\)
    \[\Rightarrow \forall v\in V: f(v) 
      = f\left( \sum\limits_{i=1}^n \lambda_i v_i \right)
      = \sum\limits_{i=1}^n \lambda_i f\left(v_i\right)
      = \sum\limits_{i=1}^n \lambda_i w_i
      = \sum\limits_{i=1}^n \lambda_i g\left(v_i\right)
      = g\left(\sum\limits_{i=1}^n \lambda_i v_i\right)
      = g(v)\]
\end{itemize}

\subsubsection{Satz 4.2}
Seien \(V\) und \(W\) K-Vektorräume und 
\(\left\{v_1, \hdots,  v_n\right\}\) eine Basis von \(V\). 
Eine lineare Abbildung \(f: V \rightarrow W \) ist genau dann ein
Isomorphismus, wenn die Bilder der Basisvektoren 
\(\left\{f(v_1), \hdots, f(v_n)\right\}\) eine Basis vom Vektorraum
\(W\) bilden.

\subsubsection{Beweis}
\[
  f \text{ bijektiv} \Rightarrow 
  \begin{cases}
    f \text{ injektiv} &\xLeftrightarrow{\text{ÜB6HA1b}} f(v_1), \hdots, f(v_n) 
    \text{ linear unabhängig}\\
    f \text{ surjektiv} & \Leftrightarrow f(v_1), \hdots, f(v_n) 
    \text{ EZS von } W\\
  \end{cases}
  \Rightarrow \left\{f(v_1), \hdots, f(v_n)\right\} 
  \text{Basis von }V
\]

\subsubsection{Bemerkungen}
\begin{enumerate}
\item Seien \(\left\{v_1, \hdots, v_n\right\}\) Basis von \(V\) 
  und \(\left\{w_1, \hdots, w_n\right\}\) Basis von \(W\).\\
  \(\xRightarrow{S4.1, S4.2} f(v_i) = w_i, i=1, \hdots, n\) definiert
  durch eine \emph{eindeutige} 
  \(\underbrace{\text{\emph{bijektive, lineare}}}_{\text{Isomorphismus}}\) 
  Abbildung \(f: V\rightarrow W\)
\item \(V\) und \(W\) sind isomorph 
  (\(V\cong W)) \Leftrightarrow dim(V)= dim(W)\)\\
  {\bf Beweis:} \(\Leftarrow\) aus S4.2, \(\Rightarrow\) aus 1.
\item Sei \(f: V \rightarrow W\) linear und die beiden Räume haben die
  gleiche Dimension, \(dim(V) = dim(W) = n\)\\
  Dann gilt \(f \text{ injektiv} \Leftrightarrow f\text{ surjektiv}\)\\
  {\bf Beweis:} Seien \(V, W\) K-Vektorräume mit \(dim(V) = dim(W) =
  n\) und 
  sei \(f: V\rightarrow W\) Abbildung und \(f\) injektiv.
  \begin{align*}
    \Leftrightarrow
    &dim(Kern(f)) = 0\\
    \Leftrightarrow
    &dim(Bild(f)) = n = dim(W)\\
    \Leftrightarrow
    &f \text{ surjektiv }
  \end{align*}    
\item Jeder n-dimensionale K-Vektorraum ist isomorph zu \(K^n\)\\
  {\bf Beweis:} Seien \(B = \left\{v_1, \hdots, v_n\right\}\) eine Basis von
    \(V\) und \(\left\{e_1, \hdots, e_n\right\}\) kanonische Basis von
    \(K^n\)
    \begin{align*}
      \xRightarrow{S4.1}
      &\exists! \varphi_B: V \rightarrow K^n 
        \text{ linear mit } \varphi_B(v_i) = e_i, i = 1, \hdots, n\\
      \Rightarrow & \varphi_B \text{  Isomorphismus }V\cong K^n
    \end{align*}
  {\bf Man beachte:} Sei 
    \(v \in V \Rightarrow v = \sum\limits_{j=1}^n \lambda_j v_j \in V\) 
    beliebig
    \[\Rightarrow \varphi_B (v) 
      = \varphi_B\left(\sum\limits_{j=1}^n \lambda_j v_j\right)
      = \sum\limits_{j=1}^n \lambda_j \varphi_B(v_j)
      = \sum\limits_{j=1}^n \lambda_j e_j
      = 
      \begin{bmatrix}
        \lambda_1\\
        \vdots\\
        \lambda_n\\
      \end{bmatrix}
    \]
\end{enumerate}

\subsection{Koordinatenabbildung}
\subsubsection{Definition}
Sei \(V\) ein K-Vektorraum und \(B = \left\{v_1, \hdots, v_n\right\}\) eine
  Basis von \(V\).\\
Eine lineare bijektive Abbildung 
\[\varphi_B: V \rightarrow K^n
  \text{ mit }
 \varphi_B(v)= \begin{bmatrix}\lambda_1\\\vdots\\\lambda_n\end{bmatrix}\]
für \(v = \sum\limits_{i=1}^n \lambda_i v_i, v\in V\) heißt
\emph{Koordinatenabbildung} von \(V\) bezüglich \(B\). 
\(\lambda_1, \hdots, \lambda_n\) heißen die Koordinaten von \(v\)
bzgl. \(B\), 
\(\varphi_B(v)
= \begin{bmatrix}\lambda_1\\\vdots\\\lambda_n\end{bmatrix}\) 
heißt Koordinatenvektor von \(v\) bzgl \(B\).
\subsubsection{Bemerkung}
\[\exists\varphi_B^{-1}: K^n \rightarrow V\
  \text{ mit } \varphi_B^{-1}(e_j) =v_j , j = 1, \hdots, n\]



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Lineare_Algebra_1"
%%% End:
