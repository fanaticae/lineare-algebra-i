\section{Gruppen}
\subsection{Verknüpfung}
\subsubsection{Definition}
Sei \(G\neq \varnothing\) eine Menge.\\
Sei \(\circ\) eine Abbildung:
\begin{align*}
  \circ: G\times G &\rightarrow G\\
  (a, b)&\mapsto a \circ b
\end{align*}
dann hat \(G\) die Verknüpfung \(\circ\).
\subsection{Gruppe}
\subsubsection{Definition}
Sei \(G\neq \varnothing\) eine Menge mit der Verknüpfung \(\circ\).\\
Das Paar \((G, \circ)\) heißt Gruppe, falls:
\begin{itemize}
  \item[\((A)\)] \(\circ\) ist assoziativ, dh. 
    \(\forall a,b,c \in G: (a \circ b) \circ c = a \circ (b \circ c)\)
  \item[\((N)\)] Es gibt ein Element \(e\in G\), genannt 
    \emph{neutrales Element}, sodass 
    \(\forall a \in G: e \circ a = a\). 
  \item[\((I)\)] Zu jedem Element \(a\in G \) gibt es ein Element \(a'\in G\),
    genannt \glqq Inverses Element\grqq , sodass 
    \(a' \circ a = e\). (Geschrieben \(a^{-1}\) statt \(a'\)). 
\end{itemize}

\subsubsection{Beispiele}
\begin{center}
  \begin{tabular}{|c|c|c|p{6cm}|}
    \hline
    {\bf Paar} & {\bf Neutrales Element} & {\bf Inverses Element} &{\bf Kommentar}\\
    \hline
    \((\mathbb{Z},+)\) & \(0\) & \(-a\) & Gruppe\\
    \hline
    \((\mathbb{Z},\cdot)\)& 1 & & Keine Gruppe, da kein Inverses
                                     Element.\\
    \hline
    \((\mathbb{Q}\backslash\{0\}, \cdot)\)& \(1\) & \(\frac{1}{a}\) & Gruppe\\
    \hline
  \end{tabular}
\end{center}

\subsubsection{Satz}
Sei \((G,\circ) \) eine Gruppe, dann gilt:
\begin{enumerate}
  \item Ist \(a'\in G\) das inverse Element zu \(a \in G\), so gilt:
    \[a \circ a' = e = a' \circ a\]
  \item \(\forall a \in G: a\circ e = a\)
  \item Es gibt genau ein neutrales Element (Eindeutigkeit) 
  \item Zu jedem \(a \in G\) gibt es genau ein inverses Element (Eindeutigkeit) 
\end{enumerate}

\subsubsection{Beweis}
\begin{enumerate}
  \item Sei \(a \in G\) und \(a'\in G\) das inverse Element zu \(a\).\\
    Sei \(a''\in G\) ein inverses Element zu \(a' \Rightarrow a''\circ
    a' = e\)
    \begin{align*}
      a\circ a' &= e\circ (a\circ a')&&(N)\\
      &=(a''\circ a')\circ (a\circ a')\\
      &=a''\circ (a'\circ (a\circ a'))&&(A)\\
      &=a''\circ ((a'\circ a)\circ a')&&(A)\\
      &=a''\circ (e\circ a') &&(I)\\
      &=a''\circ a'&& (N)\\
      &=e &&(I)
    \end{align*}
  \item Sei \(a\in G\) und \(a' \in G\) ein inverses Element zu \(a\):
    \[a \circ e = a \circ (a' \circ a) = (a \circ a') \circ a = 
      e \circ a = a\]
  \item Seien \(e, \tilde{e} \in G\) neutrale Elemente
    \[\Rightarrow e = e\circ \tilde{e} = \tilde{e}\]
  \item Seien \(a', \tilde{a}' \in G\) inverse Elemente zu \(a\in G\):
    \[\tilde{a}' = \tilde{a}' \circ e = 
      \tilde{a'} \circ (a \circ a') =
      (\tilde{a}' \circ a) \circ a' = 
      e\circ a' = a'\]
\end{enumerate}
\textqed

\subsubsection{Bemerkungen}
\begin{enumerate}
  \item Das inverse Element bezeichnen wir mit \(a^{-1}\)
  \item \(\forall a \in G: (a^{-1})^{-1} = a\).\\
    {\bf Beweis:}
    Sei \(a \in G\) beliebig:
    \begin{align*}
      &\Rightarrow a^{-1} \circ a = e = a \circ a^{-1}\\
      &\Rightarrow a \text{ ist ein inverses Element zu } a^{-1}\\
      &\Rightarrow (a^{-1})^{^{-1}} = a \text{ wegen der Eindeutigkeit der
        Inversen}
    \end{align*}
  \item \(\forall a,b\in G: (a\circ b)^{-1} = b^{-1}\circ a^{-1}\) ~\\
    {\bf Beweis:}
    Seien \(a,b \in G\):
    \begin{align*}
      (b^{-1}\circ a^{-1})\circ (a\circ b) &= b^{-1} \circ (a^{-1}\circ (a
                                             \circ b))&&(A)\\
                                           &=b^{-1}\circ ((a^{-1}\circ a)\circ b) &&(A)\\
                                           &=b^{-1}\circ (e\circ b) &&(N)\\
                                           &=b^{-1}\circ b&& (I)\\
                                           &=e&&\\
      \Rightarrow (b^{-1}\circ a^{-1}) &= (a\circ b)^{-1}
    \end{align*}
\end{enumerate}

\subsubsection{Satz}
Sei \(M \neq \varnothing\) eine Menge, 
\(S(M) = \{f: M\rightarrow M| f \text{ bijektiv}\}\) und
\begin{align*}
\circ: S(M) \times S(M) &\rightarrow S(M)\\
(f,g) &\mapsto f\circ g &((f\circ g)(x) = f(g(x)))
\end{align*}
Dann ist \((S(M),\circ )\) eine Gruppe.
\subsubsection{Beweis}
\begin{itemize}
  \item \(\circ\) ist eine Verknüpfung, da \(f\circ g\) bijektiv ist,
    falls \(f\) und \(g\) bijektiv sind.
  \item Die Assoziativität gilt, da sie für die Komposition von Abbildungen gilt
  \item Neutrales Element \(e = id_M\)
    \[
      \forall f \in S(M): id_M \circ f = f\circ id_M = f
    \]
  \item Inverses Element folgt aus der Bijektivität der Abbildungen:
    \[
      \forall f \in S(M) \exists! f^{-1}: f \circ f^{-1} = id_M
    \]
\end{itemize}
\(\Rightarrow (S(M), \circ )\) "= Gruppe
\textqed

\subsection{Kommutativität}
\subsubsection{Definition}
Eine Gruppe \((G,\circ )\) heißt \emph{abelsch} oder kommutativ, falls:
\[\forall a,b \in G: a\circ b = b \circ a \]

\subsubsection{Beispiele}
\begin{tabular}{p{2cm} p{6cm}}
\((\mathbb{Z},+)\) & kommutative Gruppe\\
\((\mathbb{Q},\cdot)\) & kommutative Gruppe \\
\((S(M) , \circ)\) & nicht kommutative Gruppe \\
\end{tabular}

\subsubsection{Beispiel}
Sei \(M = \{1,2,3,\dots,n\}\) eine endliche Menge und 
\(S = S(M) = \{\sigma: M\rightarrow M| \sigma \text{ bijektiv}\}\) 
mit  \(\sigma\) als eine \(n\)-stellige Permutation.\\
\(
  \begin{array}{c|c}
    n & \sigma (x)\\
    \hline
    1 & \sigma(1)\\
    2 & \sigma(2)\\
    3 & \sigma(3)\\
    \vdots&\vdots\\
    n & \sigma(n)\\
  \end{array}
\) ~\\

Für \(n=3\):
\[
  \begin{array}{|c|c|}
    n & \sigma_1(x)\\
    \hline
    1 & 2\\
    2 & 1\\
    3 & 3\\
  \end{array}\,
  \begin{array}{|c|c|}
    n & \sigma_2(x)\\
    \hline
    1 & 3\\
    2 & 2\\
    3 & 1\\
  \end{array}\,
  \begin{array}{|c|c|c|}
    n & (\sigma_1\circ \sigma_2) (x)& (\sigma_2\circ \sigma_1)(x)\\
    \hline
    1 & 3 & 2\\
    2 & 1 & 3\\
    3 & 2 & 1\\
  \end{array}
\]
\(\Rightarrow (S_n, \circ)\) ist nicht kommutativ für \(n\geq 3\)

\subsection{Gruppenhomomorphismus, Gruppenisomorphismus}
\subsubsection{Definition}
Seien \((G,\circ), (H,\circ ')\) Gruppen. 
Eine \emph{Abbildung} \(\varphi: G\rightarrow H\) heißt 
\emph{Gruppenhomomorphismus}, falls
\[
  \forall a, b \in G: \varphi(a \circ b) = 
  \varphi(a) \circ' \varphi(b)
\]
Ist \(\varphi\) außerdem bijektiv, so heißt \(\varphi\) \emph{Gruppenisomorphismus}.

\subsubsection{Bemerkungen}
Ist \(\varphi: G\rightarrow H\) ein Gruppenhomomorphismus, so gilt:
\begin{enumerate}
  \item \(\varphi(e_G) = e_H\).\\
    {\bf Beweis:}
    Sei \(e_H\) das neutrale Element in \(H\) und
    \(e_G\) das Neutrale Element in \(G
    =\varphi(e_G\circ e_G) = 
    \varphi(e_G) \circ' \varphi(e_G)\) 
    das neutrale Element bezüglich \(G\).
    \begin{align*}
      e_H = (\varphi(e_G))^{-1} \circ' (\varphi(e_G))
                  &=(\varphi(e_G))^{-1} \circ' 
                    (\varphi(e_G)\circ' \varphi(e_G))\\
                  &= (\varphi(e_G)^{-1} \circ' 
                    (\varphi(e_G)) \circ' \varphi(e_G)\\
                  &= e_H \circ ' \varphi(e_G)\\
                  &= \varphi(e_G)
    \end{align*}
  \item \(\forall a \in G: \varphi(a)^{-1} = \varphi ( a^{-1} ) \) ~\\
    {\bf Beweis:}
    \begin{align*}
      \varphi(a^{-1}) \circ' \varphi(a) &=\varphi(a^{-1}\circ a)\\
                                        &=\varphi(e_G)\\
                                        &=e_H
    \end{align*}
  \item Ist \(\varphi\) bijektiv, so ist \(\varphi^{-1}: G \rightarrow H\)
    ein Gruppenisomorphismus.\\
    {\bf Beweis:}
    Seien \(c,d \in H \) und \(a,b \in G\) so, dass \(\varphi(a) = c \land \varphi(b) = d\):
    \begin{align*}
      \Rightarrow \varphi^{-1}(c\circ ' d) &=\varphi^{-1}
                                             (\varphi(a) \circ' \varphi(b))\\
                                           &=\varphi^{-1}(\varphi (a \circ b))\\
                                           &=a \circ b\\
                                           &=\varphi^{-1}(c) \circ \varphi^{-1}(d)
    \end{align*}
    \(\Rightarrow \varphi^{-1}\) ist Gruppenhomomorphismus und
    bijektiv \(\Rightarrow \varphi^{-1}\) ist ein Gruppenisomorphismus 

  \item \(G\) und \(H\) heißen isomorph, falls 
    \(\varphi: G\rightarrow H\) Gruppenisomorphismus.\\
    {\bf Beispiel:}
    \((\mathbb{R}, +),(\mathbb{R^+,\cdot}), 
    \mathbb{R}^+ := \{x\in\mathbb{R}|x>0 \}\)
   \begin{align*}
          exp:\mathbb{R}&\rightarrow \mathbb{R}^+\\
          x &\mapsto exp(x) = e^{x}
   \end{align*}
   \[\forall x,y \in G: exp(x+y) = e^{x+y} = e^x\cdot e^y = exp(x)\cdot
     exp(y)\]
   \(\Rightarrow exp\) ist Gruppenhomomorphismus und bijektiv
   \(\Rightarrow exp\) ist Gruppenisomorphismus
\end{enumerate}

\subsection{Untergruppen}
\subsubsection{Definition}
Sei \((G,\circ )\) eine Gruppe.\\
\(\varnothing\neq U\subseteq G\) heißt Untergruppe, falls:
\begin{enumerate}
  \item \(\forall a,b\in U: a\circ b \in U\)
  \item \(\forall a \in U: a^{-1} \in U\)
\end{enumerate}

\subsubsection{Satz}
Sei \((G,\circ )\) eine Gruppe und \(U\) eine Untergruppe von \(G\),
dann ist \((U,\circ)\) eine Gruppe mit der von \(G\) induzierten
Verknüpfung \(\circ: U \times U \rightarrow U\).

\subsubsection{Beweis}
Wegen 1. (Definition) ist \(\circ\) eine Verknüpfung auf \(U\).
\begin{itemize}
  \item Die {\bf Assoziativität} der induzierten Gruppe folgt aus der
    Assoziativität auf \(G\).
  \item {\bf Neutrales Element:}
    Sei \(a \in U\)
    \begin{align*}
      &\Rightarrow a^{-1} \in U &&2. \text{ (Definition)}\\
      &\Rightarrow a\circ a^{-1} = e \in U&&1. \text{ (Definition)}
    \end{align*}
    \(\Rightarrow U\) hat das selbe neutrale Element \(e\) wie \(G\).
  \item {\bf Inverses Element:}
    Sei \(a \in U \Rightarrow a^{-1} \in U\) (2. (Definition))
\end{itemize}

\subsubsection{Beispiele}
\begin{enumerate}
  \item \((\mathbb{Z},+),(\mathbb{Q},+)\) sind Untergruppen von
    \((\mathbb{R},+)\).
  \item \((\{e\},\circ),(G,\circ)\) sind Untergruppen von \((G,\circ)\)
  \item Endliche Gruppen: \\
    \fbox{
    \begin{minipage}[t]{0.2\textwidth}
      Für \(G_1 = \{e\}\):
      \[
        \begin{array}{c|c}
          \circ&e\\
          \hline 
          e & e\\
        \end{array}
      \]
    \end{minipage}
    \begin{minipage}[t]{0.3\textwidth}
      Für \(G_2 = \{a, e\}\):
      \[
        \begin{array}{c|c c}
          \circ & e & a\\
          \hline
          e & e & a\\
          a & a & e\\
        \end{array}
      \]
      Es stellt sich die Frage nach \(a \circ a\).
      Angenommen \(a \circ a = a\): 
      \begin{align*}
        a^{-1} \circ a \circ a &= a^{-1}\circ a\\ 
        e \circ a &= e\\
        a &= e \lightning_{a\neq e}
      \end{align*}
      \[\Rightarrow a \circ a = e\]
    \end{minipage}
    \begin{minipage}[t]{0.3\textwidth}
      Für \(G_3 = \{e,a,b\}\):
      \[
        \begin{array}{c|c c c}
          \circ& e & a & b\\
          \hline 
          e & e & a & b\\
          a & a & b & e\\
          b & b & e & a\\
        \end{array}
      \]
      Jedes Element ist für Zeile und Spalte eindeutig, da:\\
      Angenommen: \(a \circ b = a \circ c\).\\
      Daraus folgt:
      \begin{align*}
        (a^{-1}\circ a)\circ b &= (a^{-1}\circ a) \circ c\\
        e\circ b &= e \circ c&&\\
        b &= c&&
      \end{align*}\\

      Für \(a\circ a\):\\
      Angenommen \(a\circ a = e\):\\
      Daraus folgt: 
      \begin{align*}
        a\circ b &= b\\
        a\circ(b\circ b^{-1}) &= b\circ b^{-1}\\
        a &= e \lightning_{a\neq e}
      \end{align*}
      Somit:
      \[\Rightarrow a\circ a = b\]
      \end{minipage}
      }\\\\
      {\bf Kommutativität:}
      \[\forall a,b \in G: a\circ b = b \circ a\]
      \(\Rightarrow G_1, G_2, G_3 \) sind kommutativ.\\
      Untergruppen von \(G_3\):
      \begin{itemize}
      \item \((\{e\},\circ), (\{e,a,b\},\circ)\dots\)Untergruppen von
        \(G_3\) 
      \item \((\{e,a\},\circ)\dots\)Keine Untergruppe von \(G_3\), da
        \(a\circ a = b \notin \{e,a\}\)
        
    \end{itemize}

  \item Sei 
    \(
     m \in \mathbb{N}, 
     R = \{(x,y)\in \mathbb{Z}^2 |m \text{ teilt }x-y\}
    \) 
    eine Äquivalenzrelation.
    \[\mathbb{Z}_m = \{[0]_R, [1]_R, \dots, [m-1]_R\} = 
      \left\{
          \{mk| k\in\mathbb{Z}\},
          \{mk+1|k\in \mathbb{Z}\},
          \dots,
          \{mk+m-1|k\in \mathbb{Z}\}
      \right\} 
      = \mathbb{Z}/_R
    \]
    \begin{align*}
      +: \mathbb{Z}_m \times \mathbb{Z}_m &\rightarrow \mathbb{Z}_m\\
      ([x]_R,[y]_R)&\mapsto [x]_R + [y]_R :=[x+y]_R
    \end{align*}
    \begin{itemize}
      \item {\bf Wohldefiniertheit:}
        Seien \(x, x', y, y' \in \mathbb{Z}\) mit \([x]_R = [x']_R, [y]_R = [y']_R\).\\
        \(z.z.: [x+y]_R = [x'+y']_R\)
        
        Nach Definition:
        \begin{align*}
          m|(x-x') \land m|(y-y') 
          &\Rightarrow m|((x-x')+(y-y')) = (x+y) - (x'-y')\\
          &\Rightarrow [x+y]_R = [x' + y']_R
        \end{align*}
      \item {\bf Assoziativität:}
        \[
          ([x]_R + [y]_R) + [z]_R = 
          [x+y]_R + [z]_R = 
          [(x+y)+z]_R =
          [x+(y+z)]_R = 
          [x]_R + [y+z]_R = 
          [x]_R+ ([y]_R +[z]_R)
        \]
      \item {\bf Neutrales Element:} \(e = [0]_R\)
        \[\forall x\in \mathbb{Z}: [0]_R + [x]_R = [0+x]_R = [x]_R\]
      \item Inverses Element:\([x]_R^{-1} = [-x]_R\)
        \[[-x]_R + [x]_R = [-x+x]_R = [0]_R\]
        
      \item {\bf Kommutativität:}
        \[\forall [x]_R,[y]_R \in \mathbb{Z}: [x]_R + [y]_R = [x+y]_R =
        [y+x]_R = [y]_R + [x]_R\]
    \end{itemize}
    \(\Rightarrow (\mathbb{Z}_m, +)\) - kommutative Gruppe mit \(m\) Elementen.
\end{enumerate}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: "../../Lineare_Algebra_1"
%%% End:
