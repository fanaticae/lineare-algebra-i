**UMFRAGE Zeilenabstand**   
~~Soll der Zeilenabstand so bleiben wie er ist, oder erhöht, sogar verdoppelt werden ? DU ENTSCHEIDEST~~  
~~http://goo.gl/forms/IuYiynhNQB~~   
Danke für die Teilnahme, die Ergebnisse findet ihr [hier](https://docs.google.com/spreadsheets/d/1ZBVH40007aCeR-hoZsuFa2uESJ8Fqc87lvO1yeEuxmY/edit?usp=sharing)

Dies ist meine Mitschrift zu Lineare Algebra 1 (WS 15/16). 

Mir ist bekannt und bewusst, dass sowohl Formatierung als auch Inhalt teilweise überarbeitet gehören. Auch das fehlen eineiger (fast aller) Grafiken ist mir bekannt. 

Sollte jemand die Muse, viel Langeweile oder ein starkes Bedürfnis zur Korrektur dieser Probleme erleben, so ist er/sie/es gerne dazu eingeladen, diesem nachzukommen. Besonders das erstellen der fehlenden Grafiken käme mir sehr gelegen, da mein Talent dazu kaum bis garnicht vorhanden ist. 
Sollten euch Fehler auffallen, so könnt ihr gern ein Ticket im Bugtracker erstellen, sodass ich oder irgendjemand mit viel Zeit/Muse,... zumindest weiß, wo er/sie/es ansetzen kann. Lieber wäre es mir natürlich, wenn ihr selbst diesen Fehler behebt. 

Sollte jemand sich tatsächlich erbarmen und einen gefundenen Fehler, ein Formatierungsproblem, etc entdecken und beheben, so erstelle er/sie/es doch bitte einen Pull-Request mit der Korrektur. Ich bitte euch, dass ihr euch bei der Korrektur zumindest an den Stil des Dokuments lehnt, auch wenn er nicht der schönste ist. 


In allem weiteren wünsche ich viel Spaß 


Die Verwendung ist lediglich zur privaten Nutzung erlaubt. 
Die Korrektheit des Inhaltes wird nicht garantiert.

**Anmerkung: Das Script findet ihr unter Source, nicht unter Download, da unter Source das Skript jedes Mal selbstständig upgedated wird, unter Downloads wäre das noch zusätzlicher Aufwand.**